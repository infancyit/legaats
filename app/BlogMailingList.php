<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
class BlogMailingList extends Model
{
    public static function isAlreadySignedUp(){
    	return (BlogMailingList::whereUserId(Auth::user()->id)->first()) ? true : false;
    }

    public function user(){
    	return $this->belongsTo('App\\User');
    }
}
