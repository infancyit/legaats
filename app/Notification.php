<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
class Notification extends Model
{
    public function userWho(){
    	return $this->belongsTo('App\\User', 'who');
    }


    public static function getNotifications()
    {
        // $notifications = Notification::with('userWho')->whereUserId(Auth::user()->id)->orderBy('id', 'desc')->get();
        $notifications = Notification::with('userWho')->whereUserId(Auth::user()->id)->whereStatus('unseen')->orderBy('id', 'desc')->get();
        return $notifications;
    }
}
