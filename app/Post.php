<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
class Post extends Model
{
    public function user(){
    	return $this->belongsTo('App\\User');
    }
    public function image(){
    	return $this->hasOne('App\\Photo');
    }

    public function scrape(){
    	return $this->hasOne('App\\Scrape');
    }

    public function categories(){
        return $this->hasMany('App\\PostCategory');
    }
    public function comments(){
        return $this->hasMany('App\\Comment');
    }

    public static function getSideBarInformation(){


    	$categories = Category::whereUserId(null)->get();

    	foreach ($categories as $category) {
    		$post_ids = PostCategory::whereCategoryId($category->id)->lists('post_id');
            $category->posts = Post::with('user','image','scrape')->whereIn('id', $post_ids)->orderBy('id', 'desc')->take(5)->get();

    	}

        return $categories;
    }


    public static function processGeneralPost($post){

        $user = Auth::user()->id;


        // Like Calculation
        $isLiked = Like::whereUserId($user)->wherePostId($post->id)->first();
        $post->isLiked = ($isLiked) ? $isLiked->id : false;
        $post->likeCount = Like::wherePostId($post->id)->get()->count();

        $post->isFollowUser = FollowUser::whereUserId($user)->whereFollowing($post->user_id)->first();

        
        // // Upvote Calculation
        // $isUpvoted = Upvote::whereUserId($user)->wherePostId($post->id)->first();
        // $post->isUpvoted = ($isUpvoted) ? $isUpvoted->id : false;
        // $post->upvoteCount = Upvote::wherePostId($post->id)->get()->count();
        
        // // Down Vote Calculation
        // $isDownvoted = Downvote::whereUserId($user)->wherePostId($post->id)->first();
        // $post->isDownvoted = ($isDownvoted) ? $isDownvoted->id : false;
        // $post->downvoteCount = Downvote::wherePostId($post->id)->get()->count();
        
        // Comment  Calculation
        $post->commentCount = count($post->comments);
        
        $savedPost = SavedPost::wherePostId($post->id)->whereUserId($user)->first();
        $queuedPost = Queue::wherePostId($post->id)->whereUserId($user)->first();

        $post->isOwner = ($post->user_id == $user) ? true : false;
        $post->isSaved = ($savedPost == null) ? false : true;
        $post->isQueued = ($queuedPost == null) ? false : true;

        $categories = [];
        foreach ($post->categories as $category) {
            $categories[] = $category->category->name; 
        }


        if($post->scrape){
            if(strpos($post->scrape->link, 'youtube.com/watch') != false){
                
                $post->scrape->type = 'youtube';
                $parse = parse_url($post->scrape->link);
                parse_str($parse['query'], $params);
                $post->scrape->youtube = $params['v'];

            }
            else if(strpos($post->scrape->link, 'ted.com') != false){
                $post->scrape->type = 'ted';
                $post->scrape->ted = preg_replace('/www/', 'embed-ssl', $post->scrape->link);
            }
            else
            {
                $post->scrape->type = 'normal';
            }
        }
        $post->allCategories = implode(' , ', $categories);
        return $post;
    }
    public static function processQueuePost($post){
        $user = Auth::user()->id;
        
        $isLiked = Like::whereUserId($user)->wherePostId($post->id)->first();
        $post->isLiked = ($isLiked) ? $isLiked->id : false;
        $post->likeCount = Like::wherePostId($post->id)->get()->count();

        $savedPost = SavedPost::wherePostId($post->id)->whereUserId($user)->first();
        $queuedPost = Queue::wherePostId($post->id)->whereUserId($user)->first();

        $post->isOwner = ($post->user_id == $user) ? true : false;
        $post->isSaved = ($savedPost == null) ? false : true;
        $post->queue_id = $queuedPost->id;
        
        $categories = [];
        foreach ($post->categories as $category) {
            $categories[] = $category->category->name; 
        }


        if($post->scrape){
            if(strpos($post->scrape->link, 'youtube.com') != false){
                
                $post->scrape->type = 'youtube';
                $parse = parse_url($post->scrape->link);
                parse_str($parse['query'], $params);
                $post->scrape->youtube = $params['v'];

            }
            else if(strpos($post->scrape->link, 'ted.com') != false){
                $post->scrape->type = 'ted';
                $post->scrape->ted = preg_replace('/www/', 'embed-ssl', $post->scrape->link);
            }
            else
            {
                $post->scrape->type = 'normal';
            }
        }
        $post->allCategories = implode(' , ', $categories);
        return $post;
    }

    public static function processProfilePost($post){
        $user = Auth::user()->id;
        
        // Like Calculation
        $isLiked = Like::whereUserId($user)->wherePostId($post->id)->first();
        $post->isLiked = ($isLiked) ? $isLiked->id : false;
        $post->likeCount = Like::wherePostId($post->id)->get()->count();
        
        // // Upvote Calculation
        // $isUpvoted = Upvote::whereUserId($user)->wherePostId($post->id)->first();
        // $post->isUpvoted = ($isUpvoted) ? $isUpvoted->id : false;
        // $post->upvoteCount = Upvote::wherePostId($post->id)->get()->count();
        
        // // Down Vote Calculation
        // $isDownvoted = Downvote::whereUserId($user)->wherePostId($post->id)->first();
        // $post->isDownvoted = ($isDownvoted) ? $isDownvoted->id : false;
        // $post->downvoteCount = Downvote::wherePostId($post->id)->get()->count();
        
        // Comment  Calculation
        $post->commentCount = count($post->comments);
        
        $savedPost = SavedPost::wherePostId($post->id)->whereUserId($user)->first();

        $post->isOwner = ($post->user_id == $user) ? true : false;
        $post->isSaved = ($savedPost == null) ? false : $savedPost->id;
        
        $categories = [];
        foreach ($post->categories as $category) {
            $categories[] = $category->category->name; 
        }
        $post->allCategories = implode(' , ', $categories);

        $categories = [];
        if($post->isSaved){
            $savedPostCategoryIds = SavedPostCategory::whereSavedPostId($post->isSaved)->lists('category_id');
            $savedPostCategories = Category::whereIn('id', $savedPostCategoryIds)->get();
            foreach ($savedPostCategories as $category) {
                $categories[] = $category->name; 
            }
        }
        
        $post->savedPostCategories = implode(' , ', $categories);

        if($post->scrape){
            if(strpos($post->scrape->link, 'youtube.com') != false){
                
                $post->scrape->type = 'youtube';
                $parse = parse_url($post->scrape->link);
                parse_str($parse['query'], $params);
                $post->scrape->youtube = $params['v'];

            }
            else if(strpos($post->scrape->link, 'ted.com') != false){
                $post->scrape->type = 'ted';
                $post->scrape->ted = preg_replace('/www/', 'embed-ssl', $post->scrape->link);
            }
            else
            {
                $post->scrape->type = 'normal';
            }
        }


        return $post;
    }
}
