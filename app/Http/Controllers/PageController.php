<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// Models
use App\User;
use App\SavedPost;
use App\SavedPostCategory;
use App\Queue;
use App\Category;
use App\Post;
use App\PostCategory;


// Library
use Validator;
use Carbon\Carbon;
use Auth;
use Hash;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

  


    public function otherProfile($id)
    {
        
        $user = User::find($id);
        $timelines = UsersController::getTimeLine($id);
        $user_categories = Category::whereUserId(null)->orWhere('user_id',Auth::user()->id)->get(); 
        return view('other_profile')->withUserCategories($user_categories)
                    ->with('title', $user->fullname)->with('user', $user)->with('timelines', $timelines);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function category($category)
    {   
        $user_categories = Category::whereUserId(null)->orWhere('user_id',Auth::user()->id)->get();
        return view('home')->with('user', Auth::user())
        ->with('title','Feed')
        ->withUserCategories($user_categories);
    }



    /**
     * 
     *
     * 
     * @return \Illuminate\Http\Response
     */
    public function about()
    {
        return view('about')
        ->with('title','About');
    }
}
