<?php

namespace App\Http\Controllers;


use App\Post;
use App\ShareViaEmail;

// Library
use Validator;
use Carbon\Carbon;
use Auth;
use Hash;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ShareViaEmailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $post = Post::find($id);
        return view('shareEmail.create')->with('post', $post);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules =[
            'post_id'  => 'required',
            'email'    => 'required|email'
        ];
        $data = $request->all();
        $validation = Validator::make($data,$rules);

        if($validation->fails()){
            return $this->responseError($validation->messages());
        }
        $post = Post::find($data['post_id']);


        $shareViaEmail = new ShareViaEmail;
        $shareViaEmail->post_id = $data['post_id'];
        $shareViaEmail->user_id = Auth::user()->id;
        $shareViaEmail->email = $data['email'];
        $shareViaEmail->save();
        
        $data['email'] = $shareViaEmail->email;
        $data['subject'] = str_limit($post->text, 70);
        $data['body'] = Auth::user()->fullname.'shared with you a post want to see <a target="_blank" href="{{route(\'single\', $post->id)}}">Click Here</a>'; 
        return $this->responseMessage($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

     public function responseMessage($message, $status_code = 200)
    {
        $response = [
            'data' => $message,
            'http_status' => $status_code
        ];
        return response()->json($response, $status_code);
    }
    public function responseError($message, $status_code = 400)
    {
        $response = [
            'error' => $message,
            'http_status' => $status_code
        ];
        return response()->json($response, $status_code);
    } 
}
