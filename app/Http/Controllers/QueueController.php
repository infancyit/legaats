<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Post;
use App\SavedPost;
use App\Category;
use App\Queue;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Exception;
use FatalErrorException;
class QueueController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $post_ids = Queue::whereUserId(Auth::user()->id)->lists('post_id');
        $posts = Post::with('user','image','scrape','categories.category')->whereIn('id', $post_ids)->orderBy('id', 'desc')->get();

        foreach ($posts as $post) {
            $post = Post::processQueuePost($post); 
        }

        $sidebarItems = Post::getSideBarInformation();
        // return $posts;
        return view('queues')->with('user', Auth::user())
        ->with('title','For Later')
        ->with('posts',$posts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        try {
            $queue = new Queue;
            $queue->post_id = $id;
            $queue->user_id = Auth::user()->id;
            $queue->save();
            return $this->responseMessage('Success');
        } catch (Exception $e) {
           
            return $this->responseError('Bad Request');
        }
        
        
      
           
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $queue = Queue::find($id);
            if($queue == null){
                throw new Exception("Bad Request", 1);
            }
            $queue->delete();

            return $this->responseMessage('Success');
        } catch (Exception $e) {
            return $this->responseError('Something Went Wrong');
        }
    }

    public function responseMessage($message, $status_code = 200)
    {
        $response = [
            'data' => $message,
            'http_status' => $status_code
        ];
        return response()->json($response, $status_code);
    }
    public function responseError($message, $status_code = 400)
    {
        $response = [
            'error' => $message,
            'http_status' => $status_code
        ];
        return response()->json($response, $status_code);
    }     
}
