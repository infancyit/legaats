<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// Models
use App\User;
use App\Blog;

// Library
use Validator;
use Carbon\Carbon;
use Auth;
use Hash;


use App\Http\Requests;
use App\Http\Controllers\Controller;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $posts = Blog::orderBy('id', 'desc')->get();
        return view('blog.index')->with('user', Auth::user())
        ->with('title','Blog')
        ->with('posts',$posts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('blog.create')->with('user', Auth::user())
        ->with('title','Create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules =[
            'title'                     => 'required',
            'description'               => 'required'
        ];
        $data = $request->all();


        $validation = Validator::make($data,$rules);

        if($validation->fails()){
            return redirect()->back()->withErrors($validation);
        }
        $post = new Blog;
        $post->title = $data['title'];
        $post->description = $data['description'];
        $post->save();
        return redirect()->route('blog.index')->withSuccess("Created Successfully");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Blog::find($id);
        return view('blog.show')->with('user', Auth::user())
        ->with('title','Details')->with('post', $post);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Blog::find($id);
        return view('blog.edit')->with('user', Auth::user())
                 ->with('title','Edit')->with('post', $post);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'title'                     => 'required',
            'description'               => 'required'
        ];
        $data = $request->all();


        $validation = Validator::make($data,$rules);

        if($validation->fails()){
            return redirect()->back()->withErrors($validation);
        }
        $post = Blog::find($id);
        $post->title = $data['title'];
        $post->description = $data['description'];
        $post->save();
        return redirect()->route('blog.index')->withSuccess("Updated Successfully");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Blog::find($id);
        $post->delete();
        return redirect()->route('blog.index')->withSuccess("Deleted Successfully");
    }
}
