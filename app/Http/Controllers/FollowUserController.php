<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\FollowUser;
use App\User;

use Validator;
use Carbon\Carbon;
use Auth;
use Hash;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class FollowUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules =[
            'user_id'                     => 'required',
        ];
        $data = $request->all();
        $validation = Validator::make($data,$rules);

        if($validation->fails()){
            return $this->responseError($validation->messages());
        }

        $follow_user = new FollowUser;
        $follow_user->user_id = Auth::user()->id;
        $follow_user->following = $data['user_id'];
        $follow_user->save();
        return $this->responseMessage($follow_user);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $follow_user = FollowUser::find($id);
        $follow_user->delete();

        return $this->responseMessage('success');

    }
    
    public function responseMessage($message, $status_code = 200)
    {
        $response = [
            'data' => $message,
            'http_status' => $status_code
        ];
        return response()->json($response, $status_code);
    }
    public function responseError($message, $status_code = 400)
    {
        $response = [
            'error' => $message,
            'http_status' => $status_code
        ];
        return response()->json($response, $status_code);
    }
}
