<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SavedPost;
use App\SavedPostCategory;
use App\Http\Requests;
use App\Queue;
use App\Http\Controllers\Controller;
use Auth;
use Exception;
use Validator;

class SavedPostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules =[
            'post_id'                  => 'required',
            'category_id'              => 'required'
        ];
        $data = $request->all();  

        $validation = Validator::make($data,$rules);

        if($validation->fails()){
            return $this->responseError($validation->messages());
        }

        try {
            $savedPost = new SavedPost;

            $savedPost->post_id = $data['post_id'];
            $savedPost->user_id = Auth::user()->id;
            $savedPost->save();
            foreach ($data['category_id'] as $value) {
                $savedPostCategory = new SavedPostCategory;
                $savedPostCategory->saved_post_id = $savedPost->id;
                $savedPostCategory->category_id = $value;
                $savedPostCategory->save();   
            }
            if(isset($data['queue_id'])){
                $queue = Queue::find($data['queue_id']);
                $queue->delete();
            }
        


            return $this->responseMessage('Success');
        } catch (Exception $e){
            return $this->responseError('Something Went Wrong'.$e->getMessage());
        }
        


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         try {
            $savedPost = SavedPost::find($id);
            if($savedPost == null){
                throw new Exception("Bad Request", 1);
            }
            $savedPost->delete();

            return $this->responseMessage('Success');
        } catch (Exception $e) {
            return $this->responseError('Something Went Wrong');
        }
    }

    public function responseMessage($message, $status_code = 200)
    {
        $response = [
            'data' => $message,
            'http_status' => $status_code
        ];
        return response()->json($response, $status_code);
    }
    public function responseError($message, $status_code = 400)
    {
        $response = [
            'error' => $message,
            'http_status' => $status_code
        ];
        return response()->json($response, $status_code);
    } 
}
