<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\SignUp;

use App\SavedPost;
use App\SavedPostCategory;
use App\Queue;
use App\FollowCategory;
use App\Category;
use App\Post;
use App\PostCategory;
use App\FollowUser;
use App\BlogMailingList;
//Lib

use Validator;
use Carbon\Carbon;
use Auth;
use Hash;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Socialite;
use Laravel\Socialite\Two\InvalidStateException;
class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = SignUp::all();
        return view('admin.user.index')
        ->with('title', 'All Users')
        ->with('users', $users);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function whatNew()
    {  

        $wallType = 'new';

        return view('home')->with('user', Auth::user())
        ->with('wallType',$wallType)
        ->with('title','Feed');
    }

    public function whatYouFollow()
    {  

        $wallType = 'follow';
        
        return view('home')->with('user', Auth::user())
        ->with('wallType',$wallType)
        ->with('title','Feed');
    }

    public function category($id)
    {  

        $category = Category::find($id);
 

        $followUser = FollowCategory::whereUserId(Auth::user()->id)->whereCategoryId($category->id)->first();

        return view('category')->with('user', Auth::user())->with('followUser', $followUser)
        ->with('title','Category')->with('category', $category);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {
            $facebook_user = Socialite::with('facebook')->user();

            $user = User::whereUid($facebook_user->getId())->first();
            if($user == null){
                $user = new User;
            }
            $user->uid = $facebook_user->getId();
            $user->fullname = $facebook_user->getName();
            $user->email = $facebook_user->getEmail();
            $user->fb_access_token = $facebook_user->token;
            $user->avatar = $facebook_user->getAvatar();
            // return $facebook_user->getImage();
            $user->save();

            $blogMailingList = BlogMailingList::whereUserId($user->id)->first();

            if($blogMailingList == null && $user->email != null){
                $blogMailingList = new BlogMailingList;
                $blogMailingList->user_id = $user->id;
                $blogMailingList->email = $user->email;
                $blogMailingList->status = 'Accepted';
                $blogMailingList->save();
            }
            

            if (Auth::login($user))
            {
                return redirect()->route('wall');
            } 
            return redirect()->route('login')
                        ->withInput()
                        ->withErrors('You Restrict Something that we need.');
        } catch (InvalidStateException $e) {
            return redirect()->route('login')->with('error', 'Something went wrong, please try again!');
        }
        
        // return view('auth.register')
        //             ->with('title', 'Register');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules =[
            'name'                  => 'required',
            'email'                 => 'required|unique:users,email',
            'password'              => 'required|confirmed',
            'password_confirmation' => 'required'
        ];
        $data = $request->all();

        $validation = Validator::make($data,$rules);

        if($validation->fails()){
            return redirect()->back()->withErrors($validation)->withInput();
        }else{
            $user = new User;
            $user->name = $data['name'];
            $user->email = $data['email'];
            $user->password = Hash::make($data['password']);

            if($user->save()){
                Auth::logout();
                return redirect()->route('login')
                            ->with('success','Registered successfully. Sign In Now.');
            }else{
                return redirect()->route('dashboard')
                            ->with('error',"Something went wrong.Please Try again.");
            }
        }
    }

    /**
     * Display the profile Info.
     *
     * @param  none
     * @return \Illuminate\Http\Response
     */
    public function profile()
    {
        
      
        $timelines = UsersController::getTimeLine(Auth::user()->id);

        return view('profile')
                    ->with('title', 'Profile')
                    ->with('timelines', $timelines)
                    ->with('user', Auth::user());

    }

    public static function getTimeLine($user_id){
        $posts = Post::whereUserId($user_id)->orderBy('id', 'desc')->get();
        // $posts = Post::orderBy('id', 'desc')->get();

        $years = [];
        $i = 0;

        foreach ($posts as $post) {
            $timestamp = Carbon::parse($post->created_at);
            $year = $timestamp->year;
            $month =  $timestamp->month;
            if($i > 0){
                
                if($years[$i-1]->year == $year){
                    if($years[$i-1]->months[sizeof($years[$i-1]->months) - 1] != $month){
                        $years[$i-1]->months[] = $month;
                    }
                }
                else{
                    $years[$i] = (object) [];
                    $years[$i]->year = $year;
                    $years[$i]->months = [$month]; 
                    $i++;
                }
            }
            else{
                $years[$i] = (object) [];
                $years[$i]->year = $year; 
                $years[$i]->months = [$month]; 
                $i++;   
            }
           
            
        }
        return $years;
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = SignUp::find($id);
        $user->delete();

         return redirect()->route('admin.user.index')
                            ->with('success','Deleted Successfully');
    }



    public function testLogin($id)
    {
        $user = User::find($id);

        Auth::login($user); 
        return redirect()->route('dashboard');
    }

    public function mailings()
    {
        $users = User::all();

        foreach ($users as $user) {
            $blogMailingList = BlogMailingList::whereUserId($user->id)->first();

            if($blogMailingList == null && $user->email != null){
                $blogMailingList = new BlogMailingList;
                $blogMailingList->user_id = $user->id;
                $blogMailingList->email = $user->email;
                $blogMailingList->status = 'Accepted';
                $blogMailingList->save();
            }
        }

       
        return 'done';
    }

   
}
