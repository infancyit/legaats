<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Downvote;
use App\User;

use Validator;
use Carbon\Carbon;
use Auth;
use Hash;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class DownvoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $downvotes = Downvote::with('user')->wherePostId($id)->get();
        $downvotedPeople = "";
        foreach ($downvotes as $downvote) {
           $downvotedPeople = $downvotedPeople.$downvote->user->fullname.'<br>';
        }
        return $this->responseMessage($downvotedPeople);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules =[
            'post_id'                     => 'required',
        ];
        $data = $request->all();
        $validation = Validator::make($data,$rules);

        if($validation->fails()){
            return $this->responseError($validation->messages());
        }

        $downvote = Downvote::whereUserId(Auth::user()->id)->wherePostId($data['post_id'])->first();
        if($downvote){
            return $this->responseError('Already Liked');
        }

        $downvote = new Downvote;
        $downvote->post_id = $data['post_id'];
        $downvote->user_id = Auth::user()->id;
        $downvote->save();
        return $this->responseMessage($downvote);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $downvote = Downvote::find($id);
        $downvote->delete();

        return $this->responseMessage('success');
    }

    public function responseMessage($message, $status_code = 200)
    {
        $response = [
            'data' => $message,
            'http_status' => $status_code
        ];
        return response()->json($response, $status_code);
    }
    public function responseError($message, $status_code = 400)
    {
        $response = [
            'error' => $message,
            'http_status' => $status_code
        ];
        return response()->json($response, $status_code);
    }
}
