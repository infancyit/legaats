<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Post;
use App\BlogMailingList;
use App\Category;
use App\User;

// Library
use Validator;
use Carbon\Carbon;
use Auth;
use Hash;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class BlogMailingListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mailings = BlogMailingList::with('user')->get();
        $timelines = UsersController::getTimeLine(Auth::user()->id);
        $total_user_count =  User::get()->count();
        return view('blogMailList.index')->with('user', Auth::user())
        ->with('total_user_count', $total_user_count)
        ->with('title','Mailing List')->with('mailings', $mailings)
        ->with('timelines' , $timelines);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules =[
            'email'    => 'required|email'
        ];
        $data = $request->all();
        $validation = Validator::make($data,$rules);

        if($validation->fails()){
            return redirect()->back()->withErrors($validation);
        }



        $blogMailingList = new BlogMailingList;
        $blogMailingList->user_id = Auth::user()->id;
        $blogMailingList->email = $data['email'];
        $blogMailingList->status = 'pending';
        $blogMailingList->save();

        return redirect()->back()->withSuccess("Successfully Signed Up");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $blogMailingList = BlogMailingList::find($id);
        $blogMailingList->delete();
        return redirect()->route('signup.blog.mail.index')->withSuccess("Deleted Successfully");
    }


    
    public function accept($id)
    {
        $blogMailingList = BlogMailingList::find($id);
        $blogMailingList->status = 'Accepted';
        $blogMailingList->save();
        return redirect()->route('signup.blog.mail.index')->withSuccess("Accepted");

    }
    public function responseMessage($message, $status_code = 200)
    {
        $response = [
            'data' => $message,
            'http_status' => $status_code
        ];
        return response()->json($response, $status_code);
    }
    public function responseError($message, $status_code = 400)
    {
        $response = [
            'error' => $message,
            'http_status' => $status_code
        ];
        return response()->json($response, $status_code);
    }
}
