<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// Models
use App\User;
use App\SavedPost;
use App\SavedPostCategory;
use App\Queue;
use App\Category;
use App\Post;
use App\PostCategory;


// Library
use Validator;
use Carbon\Carbon;
use Auth;
use Hash;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }


    public function search(Request $request)
    {
        $rules =[
            'key'    => 'required'
        ];
        $data = $request->all();
        $validation = Validator::make($data,$rules);
        if($validation->fails()){
            return $this->responseError($validation->messages());
        }

        $categories = Category::where('name' , 'like', '%'.$data['key'].'%')->paginate(5);

        $users = User::where('fullname' , 'like', '%'.$data['key'].'%')->paginate(5);
     
        $data = [
            'categories' => $categories->toArray()['data'],
            'users' => $users->toArray()['data']
        ];

        return $this->responseMessage($data); 
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules =[
            'name'    => 'required|unique:categories,name'
        ];


        $data = $request->all();
        $data['url'] = preg_replace('/\s+/', '_', $data['name']);
        
        $validation = Validator::make($data,$rules);

        if($validation->fails()){
            return $this->responseError($validation->messages());
        }

        $category = new Category;
        $category->name = $data['name'];
        $category->url = $data['url'];
        $category->user_id = Auth::user()->id;
        $category->save();
        return $this->responseMessage($category);
        
           
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules =[
            'name'    => 'required|unique:categories,name'
        ];


        $data = $request->all();
        $data['url'] = preg_replace('/\s+/', '_', $data['name']);
        
        $validation = Validator::make($data,$rules);

        if($validation->fails()){
            return $this->responseError($validation->messages());
        }

        $category = Category::find($id);
        $category->name = $data['name'];
        $category->url = $data['url'];
        $category->save();

        return $this->responseMessage('Something Went Wrong');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::find($id);
        $category->delete();
        return redirect()->route('profile')->withSuccess("Category Deleted Successfully");
    }


     public function responseMessage($message, $status_code = 200)
    {
        $response = [
            'data' => $message,
            'http_status' => $status_code
        ];
        return response()->json($response, $status_code);
    }
    public function responseError($message, $status_code = 400)
    {
        $response = [
            'error' => $message,
            'http_status' => $status_code
        ];
        return response()->json($response, $status_code);
    } 
}
