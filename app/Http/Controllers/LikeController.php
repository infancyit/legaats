<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Like;
use App\User;
use App\Notification;
use App\Post;

use Validator;
use Carbon\Carbon;
use Auth;
use Hash;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class LikeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $likes = Like::with('user')->wherePostId($id)->get();
        $likedPeople = "";
        foreach ($likes as $like) {
           $likedPeople = $likedPeople.$like->user->fullname.'<br>';
        }
        return $this->responseMessage($likedPeople);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules =[
            'post_id'                     => 'required',
        ];
        $data = $request->all();
        $validation = Validator::make($data,$rules);

        if($validation->fails()){
            return $this->responseError($validation->messages());
        }

        $like = Like::whereUserId(Auth::user()->id)->wherePostId($data['post_id'])->first();
        if($like){
            return $this->responseError('Already Liked');
        }

        $like = new Like;
        $like->post_id = $data['post_id'];
        $like->user_id = Auth::user()->id;
        $like->save();

        $post_owner = Post::find($like->post_id)->user_id;
        if($like->user_id != $post_owner){
            $notification = new Notification;
            $notification->user_id = $post_owner;
            $notification->type = 'like';
            $notification->post_id = $like->post_id;
            $notification->who = $like->user_id;
            $notification->like_id = $like->id;
            $notification->status = 'unseen';
            $notification->save();
        }

        return $this->responseMessage($like);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $like = Like::find($id);
        $like->delete();

        return $this->responseMessage('success');

    }

    public function responseMessage($message, $status_code = 200)
    {
        $response = [
            'data' => $message,
            'http_status' => $status_code
        ];
        return response()->json($response, $status_code);
    }
    public function responseError($message, $status_code = 400)
    {
        $response = [
            'error' => $message,
            'http_status' => $status_code
        ];
        return response()->json($response, $status_code);
    }
}
