<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// Models
use App\User;
use App\SavedPost;
use App\Photo;
use App\Queue;
use App\Category;
use App\Post;
use App\Scrape;
use App\PostCategory;
use App\SavedPostCategory;
use App\Like;
use App\Dislike;
use App\Upvote;
use App\Downvote;
use App\Comment;
use App\Notification;

//Library 
use Validator;
use DB;
use Carbon\Carbon;
use Auth;
use Hash;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class NotificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $notifications = Notification::with('userWho')->whereUserId(Auth::user()->id)->orderBy('id', 'desc')->paginate(5);

        foreach ($notifications as $notification) {
            $notification->timeline = Carbon::parse($notification->created_at)->diffForHumans();
        }
        

        $data['meta'] = [
            'current_page' => $notifications->currentPage(),
            'last_page' => $notifications->lastPage()
        ];
        $data['content'] = $notifications->getCollection();
        return $this->responseMessage($data);
    }

    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $notification = Notification::find($id);
        $notification->status = 'seen';
        $notification->save();
        return $this->responseMessage('success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }


    public function responseMessage($message, $status_code = 200)
    {
        $response = [
            'data' => $message,
            'http_status' => $status_code
        ];
        return response()->json($response, $status_code);
    }
    public function responseError($message, $status_code = 400)
    {
        $response = [
            'error' => $message,
            'http_status' => $status_code
        ];
        return response()->json($response, $status_code);
    }
}
