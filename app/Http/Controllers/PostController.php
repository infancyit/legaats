<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// Models
use App\User;
use App\SavedPost;
use App\Photo;
use App\Queue;
use App\Category;
use App\Post;
use App\Scrape;
use App\PostCategory;
use App\SavedPostCategory;
use App\Like;
use App\Dislike;
use App\Upvote;
use App\Downvote;
use App\Comment;
use App\FollowUser;
use App\FollowCategory;

//Library 
use Validator;
use DB;
use Carbon\Carbon;
use Auth;
use Hash;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Image;
use Illuminate\Support\Facades\File;
use Goutte\Client;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public $limit = 5;
    
    public function index(Request $request)
    {
     
        $posts = Post::with('user','image','scrape','categories.category','comments')->orderBy('id', 'desc')->paginate($this->limit);
        $data['meta'] = [
            'current_page' => $posts->currentPage(),
            'last_page' => $posts->lastPage()
        ];
       
        foreach ($posts as $post) {
            $post = Post::processGeneralPost($post);               
        }
        
        $response = view('posts.home')->with('posts', $posts)->with('user', Auth::user());

        $data['content'] = $response->render();
        return $this->responseMessage($data);
    }

    public function whatFollow(Request $request)
    {
       
        $user_ids = FollowUser::whereUserId(Auth::user()->id)->lists('following');
        
        $following_categories = FollowCategory::whereUserId(Auth::user()->id)->lists('category_id');

        $post_ids = PostCategory::whereIn('category_id', $following_categories)->lists('post_id');

        $posts = Post::with('user','image','scrape','categories.category','comments')->whereIn('user_id', $user_ids)->orWhereIn('id', $post_ids)->orWhere('user_id', Auth::user()->id)->orderBy('id', 'desc')->paginate($this->limit);
        $data['meta'] = [
            'current_page' => $posts->currentPage(),
            'last_page' => $posts->lastPage()
        ];
       
        foreach ($posts as $post) {
            $post = Post::processGeneralPost($post);               
        }
        
        $response = view('posts.home')->with('posts', $posts)->with('user', Auth::user());

        $data['content'] = $response->render();
        return $this->responseMessage($data);
    }

    

    public function category(Request $request, $category)
    {
        $posts = [];
        $data = $request->all();
        $category = Category::find($category);
        $post_ids = PostCategory::whereCategoryId($category->id)->lists('post_id');
            

        $desiredPosts = DB::table('posts')->select('posts.id', DB::raw('COUNT(likes.post_id) as count'))
                            ->join('likes', 'posts.id', '=', 'likes.post_id', 'left outer')->whereIn('posts.id', $post_ids)->groupBy('posts.id')->orderBy('count', 'desc')->orderBy('posts.id', 'desc')
                                     ->paginate($this->limit);
        
        foreach($desiredPosts as $desiredPost) {
            $posts[] = Post::with('user','image','scrape','categories.category','comments')->find($desiredPost->id);
        }
        $data = [];
        $data['meta'] = [
            'current_page' => $desiredPosts->currentPage(),
            'last_page' => $desiredPosts->lastPage()
        ];
     
       

        foreach ($posts as $post) {
            $post = Post::processGeneralPost($post);               
        }
        
        $response = view('posts.home')->with('posts', $posts)->with('user', Auth::user());

        $data['content'] = $response->render();
        return $this->responseMessage($data);
    }

    public function otherProfile(Request $request, $user_id, $category_id,$timeline = null)
    {

        // return $category_id.' '.$timeline;
        $posts = [];

        $post_ids = PostCategory::where('category_id', $category_id)->lists('post_id');
        if($category_id == 0){
            $post_ids = Post::whereIn('id', $post_ids)->whereUserId($user_id)->orWhere('user_id' ,$user_id)->lists('id');      
        }
        else{
            $post_ids = Post::whereIn('id', $post_ids)->whereUserId($user_id)->lists('id');
        }
     
        $saved_post_ids = SavedPostCategory::where('category_id',$category_id)->lists('saved_post_id');
        $saved_posts = SavedPost::whereIn('id', $saved_post_ids)->whereUserId($user_id)->lists('post_id');
        
        

        if($timeline == null){
            $posts = Post::with('user','image','scrape','categories.category','comments')->whereIn('id', $saved_posts)->orWhereIn('id', $post_ids)->orderBy('id', 'desc')->paginate($this->limit);
        }
        else
        {   
            $posts = Post::with('user','image','scrape','categories.category','comments')->whereIn('id', $saved_posts)->where('created_at', 'like', $timeline.'%')->orWhereIn('id', $post_ids)->where('created_at', 'like', $timeline.'%')->orderBy('id', 'desc')->paginate($this->limit);
        }
        
        
        $data['meta'] = [
            'current_page' => $posts->currentPage(),
            'last_page' => $posts->lastPage()
        ];
        
        
        foreach ($posts as $post) {
            $post = Post::processProfilePost($post);               
        }
        
        $response = view('posts.home')->with('posts', $posts)->with('user', Auth::user());

        $data['content'] = $response->render();
        return $this->responseMessage($data);
    }

    public function profile(Request $request)
    {

        $posts = [];
        $data = $request->all();
        $saved_posts = SavedPost::whereUserId(Auth::user()->id)->lists('post_id');
      
        $data = [];
        
        $posts = Post::with('user','image','scrape','categories.category','comments')->whereIn('id', $saved_posts)->orWhere('user_id', Auth::user()->id)->orderBy('id', 'desc')->paginate($this->limit);
        
        $data['meta'] = [
            'current_page' => $posts->currentPage(),
            'last_page' => $posts->lastPage()
        ];
        
        
        foreach ($posts as $post) {
            $post = Post::processProfilePost($post);               
        }
        
        $response = view('posts.profile')->with('posts', $posts)->with('user', Auth::user());

        $data['content'] = $response->render();
        return $this->responseMessage($data);
    }



    public function profileCategory(Request $request, $category_id, $timeline = null)
    {

        // return $category_id.' '.$timeline;
        $posts = [];
        $post_ids = PostCategory::where('category_id', $category_id)->lists('post_id');
        if($category_id == 0){
            $post_ids = Post::whereIn('id', $post_ids)->whereUserId(Auth::user()->id)->orWhere('user_id' ,Auth::user()->id)->lists('id');      
        }
        else{
            $post_ids = Post::whereIn('id', $post_ids)->whereUserId(Auth::user()->id)->lists('id');
        }
     
        $saved_post_ids = SavedPostCategory::where('category_id',$category_id)->lists('saved_post_id');
        $saved_posts = SavedPost::whereIn('id', $saved_post_ids)->whereUserId(Auth::user()->id)->lists('post_id');
        
        if($timeline == null){
            $posts = Post::with('user','image','scrape','categories.category','comments')->whereIn('id', $saved_posts)->orWhereIn('id', $post_ids)->orderBy('id', 'desc')->paginate($this->limit);
        }
        else
        {   
            $posts = Post::with('user','image','scrape','categories.category','comments')->whereIn('id', $saved_posts)->where('created_at', 'like', $timeline.'%')->orWhereIn('id', $post_ids)->where('created_at', 'like', $timeline.'%')->orderBy('id', 'desc')->paginate($this->limit);
        }
        
        
        $data['meta'] = [
            'current_page' => $posts->currentPage(),
            'last_page' => $posts->lastPage()
        ];
        
        
        foreach ($posts as $post) {
            $post = Post::processProfilePost($post);               
        }
        
        $response = view('posts.profile')->with('posts', $posts)->with('user', Auth::user());

        $data['content'] = $response->render();
        return $this->responseMessage($data);
    }



     public function queues(Request $request)
    {
        
        $posts = [];
        $data = $request->all();
        $post_ids = Queue::whereUserId(Auth::user()->id)->lists('post_id');
        // if(isset($data['orderBy']) && $data['orderBy']){
        //     if($data['orderBy'] == 'like'){
        //         $desiredPosts = DB::table('posts')->select('posts.id', DB::raw('COUNT(likes.post_id) as count'))
        //                         ->join('likes', 'posts.id', '=', 'likes.post_id', 'left outer')->whereIn('posts.id', $post_ids)->groupBy('posts.id')->orderBy('count', 'desc')
        //                                  ->paginate($this->limit);
        //     }
        //     else if($data['orderBy'] == 'comment'){
        //         $desiredPosts = DB::table('posts')->select('posts.id', DB::raw('COUNT(comments.post_id) as count'))
        //                         ->join('comments', 'posts.id', '=', 'comments.post_id', 'left outer')->whereIn('posts.id', $post_ids)->groupBy('posts.id')->orderBy('count', 'desc')
        //                                  ->paginate($this->limit);
        //     }
        //     else if($data['orderBy'] == 'upvote'){
        //         $desiredPosts = DB::table('posts')->select('posts.id', DB::raw('COUNT(upvotes.post_id) as count'))
        //                         ->join('upvotes', 'posts.id', '=', 'upvotes.post_id', 'left outer')->whereIn('posts.id', $post_ids)->groupBy('posts.id')->orderBy('count', 'desc')
        //                                  ->paginate($this->limit);
        //     }
        //     else if($data['orderBy'] == 'downvote'){
        //         $desiredPosts = DB::table('posts')->select('posts.id', DB::raw('COUNT(downvotes.post_id) as count'))
        //                         ->join('downvotes', 'posts.id', '=', 'downvotes.post_id', 'left outer')->whereIn('posts.id', $post_ids)->groupBy('posts.id')->orderBy('count', 'desc')
        //                                  ->paginate($this->limit);
        //     }
        //     foreach($desiredPosts as $desiredPost) {
        //         $posts[] = Post::with('user','image','scrape','categories.category','comments')->find($desiredPost->id);
        //     }
        //     $data = [];
        //     $data['meta'] = [
        //         'current_page' => $desiredPosts->currentPage(),
        //         'last_page' => $desiredPosts->lastPage()
        //     ];
        // }
        // else{
        $data = [];
        $posts = Post::with('user','image','scrape','categories.category','comments')->whereIn('id', $post_ids)->orderBy('id', 'desc')->paginate($this->limit);
        $data['meta'] = [
            'current_page' => $posts->currentPage(),
            'last_page' => $posts->lastPage()
        ];
        
        
        foreach ($posts as $post) {
            $post = Post::processQueuePost($post);               
        }
        
        $response = view('posts.queues')->with('posts', $posts)->with('user', Auth::user());

        $data['content'] = $response->render();
        return $this->responseMessage($data);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules =[
            'text'                  => 'required'
            // 'category_id' => 'required'
        ];
        $data = $request->all();


        $regex = "/(?:http|https)?(?:\:\/\/)?(?:www.)?(([A-Za-z0-9-]+\.)*[A-Za-z0-9-]+\.[A-Za-z]+)(?:\/.*)?/im";
        
        $validation = Validator::make($data,$rules);

        if($validation->fails()){
            return redirect()->back()->withErrors($validation);
        }

        preg_match_all($regex, $data['text'] , $matches);
        
        $scrapedData = null;

        if(count($matches[0]) > 0){
            $url = $matches[0][0];
            $scrapedData = (object) $this->scrape($request, $url);
        }
       


        $post = new Post;
        $post->user_id = $request->user()->id;
        $post->text = $data['text'];
        $post->save();

        if(isset($data['category_id'])){
            foreach($data['category_id'] as $category) {
                $postCategory = new PostCategory;
                $postCategory->post_id = $post->id;
                $postCategory->category_id = $category;
                $postCategory->save();
            }
        }
        

        if($request->hasFile('photo')){
            $file = $request->file('photo');
            $image = Image::make($file);
            $filename = 'uploads/post_images/'.md5(rand(1111,9999).$file->getClientOriginalName()).'.'.$file->getClientOriginalExtension(); 
            $image->save($filename);

            $photo = new Photo;
            $photo->url = $filename;
            $photo->post_id =  $post->id;
            $photo->save();
        }
        if($scrapedData){
            $scrape = new Scrape;
            $scrape->title = $scrapedData->title;
            $scrape->image = $scrapedData->image;
            $scrape->link = $scrapedData->link;
            
            $scrape->description = $scrapedData->description;
            $scrape->post_id = $post->id;
            $scrape->save();
        }
        $post = Post::with('user','image')->find($post->id);

        $post_make = view('sections.makePost')->with('post', $post)->render();

        return redirect()->to('profile')->withSuccess("Status Updated");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    
        $post = Post::with('user','image','scrape','categories.category')->find($id);
        if($post == null){
            return '';
        }
        $categories = [];
        foreach ($post->categories as $category) {
            $categories[] = $category->category->name; 
        }

        $post->allCategories = implode(' , ', $categories);
        // $post->isOwner = ($post->user_id == Auth::user()->id) ? true : false;       
        
        if($post->scrape){
            if(strpos($post->scrape->link, 'youtube.com') != false){   
                $post->scrape->type = 'youtube';
                $parse = parse_url($post->scrape->link);
                parse_str($parse['query'], $params);
                $post->scrape->youtube = $params['v'];

            }
            else if(strpos($post->scrape->link, 'ted.com') != false){
                $post->scrape->type = 'ted';
                
                $post->scrape->ted = preg_replace('/www/', 'embed-ssl', $post->scrape->link);


            }
            else
            {
                $post->scrape->type = 'normal';
            }
        }
        if(Auth::check()){
            $post = Post::processGeneralPost($post);
            $user_categories = Category::whereUserId(null)->orWhere('user_id',Auth::user()->id)->get();
            return view('single')->with('user', Auth::user())
            ->with('title','Feed')
            ->with('post',$post)
            ->withUserCategories($user_categories);
        }
        return view('usingle')
            ->with('title','Single')
            ->with('post',$post);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::with('user','image','scrape','categories.category')->find($id);
        
        $categories = [];
        foreach ($post->categories as $category) {
            $categories[] = $category->category->name; 
        }

        $post->allCategories = implode(' , ', $categories);
        $post->isOwner = ($post->user_id == Auth::user()->id) ? true : false;       
        
        if($post->scrape){
            if(strpos($post->scrape->link, 'youtube.com') != false){   
                $post->scrape->type = 'youtube';
                $parse = parse_url($post->scrape->link);
                parse_str($parse['query'], $params);
                $post->scrape->youtube = $params['v'];

            }
            else if(strpos($post->scrape->link, 'ted.com') != false){
                $post->scrape->type = 'ted';
                
                $post->scrape->ted = preg_replace('/www/', 'embed-ssl', $post->scrape->link);


            }
            else
            {
                $post->scrape->type = 'normal';
            }
        }
        // return $post;
        // return Auth::user()->id;
        $user_categories = Category::whereUserId(null)->orWhere('user_id',Auth::user()->id)->get();
        return view('editPost')->with('user', Auth::user())
        ->with('title','Edit Post')
        ->with('post',$post)
        ->withUserCategories($user_categories);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules =[
            'text'                  => 'required'
        ];
        $data = $request->all();

        $regex = "/(?:http|https)?(?:\:\/\/)?(?:www.)?(([A-Za-z0-9-]+\.)*[A-Za-z0-9-]+\.[A-Za-z]+)(?:\/.*)?/im";
        
        $validation = Validator::make($data,$rules);

        if($validation->fails()){
            return redirect()->back()->withErrors($validation);
        }

        preg_match_all($regex, $data['text'] , $matches);
        
        $scrapedData = null;

        if(count($matches[0]) > 0){
            $url = $matches[0][0];
            $scrapedData = (object) $this->scrape($request, $url);
        }
       


        $post = Post::find($id);
        $post->text = $data['text'];
        $post->save();

        $old_categories = PostCategory::wherePostId($post->id)->get();
            
        foreach ($old_categories as $old_category){
            $old_category->delete();
        }
        if(isset($data['category_id'])){

            

            foreach($data['category_id'] as $category) {

                $postCategory = new PostCategory;
                $postCategory->post_id = $post->id;
                $postCategory->category_id = $category;
                $postCategory->save();
            }
        }
        

        if($request->hasFile('photo')){
            $file = $request->file('photo');
            $image = Image::make($file);
            $filename = 'uploads/post_images/'.md5(rand(1111,9999).$file->getClientOriginalName()).'.'.$file->getClientOriginalExtension(); 
            $image->save($filename);

            $photo = Photo::wherePostId($id)->first();
            if($photo == null){
                $photo = new Photo;
                $photo->post_id = $post->id;
            }

            
            $photo->url = $filename;
            $photo->save();
        }
        if($scrapedData){

            $scrape = Scrape::wherePostId($id)->first();

            if($scrape == null){
                $scrape = new Scrape;
                $scrape->post_id = $post->id;
            }
            $scrape->title = $scrapedData->title;
            $scrape->image = $scrapedData->image;
            $scrape->link = $scrapedData->link;
            
            $scrape->description = $scrapedData->description;
           
            $scrape->save();
        }
        else{
             $scrape = Scrape::wherePostId($id)->first();
             if($scrape){
                $scrape->delete();
             }
             
        }
        $post = Post::with('user','image')->find($post->id);

        $post_make = view('sections.makePost')->with('post', $post)->render();

        return redirect()->to('profile')->withSuccess("Status Updated");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {

            $post = Post::find($id);
            $post->delete();

            return $this->responseMessage('Success');

        } catch (Exception $e) {
            return $this->responseError('Not Found');
        }
    }

    public function scrape(Request $request, $url = null){



        if($url == null)
        {
            $url = $request->get('url');
        }
        $client = new Client();
        $crawler = $client->request('GET', $url);

        if(strpos($url, 'medium.com') != false){

            $image = $crawler->filter('meta[property="og:image"]');
            $image = (count($image) == 0) ? null : $image->attr('content');

            $title = $crawler->filter('meta[property="og:title"]');
            $title = (count($title) == 0) ? null : $title->attr('content');

            $description = $crawler->filter('meta[property="og:description"]');
            $desc = (count($description) == 0) ? null : $description->attr('content');
        }
        else
        {
            $title = $crawler->filter('title');
            if(count($title) == 0)
            {
                $title = null;
            }
            else{
                 $title = $title->text();
            }
            
            $image = $crawler->filter('img');
            if(count($image) == 0)
            {
                $image = null;
            }
            else{
                $image = $image->attr('src');
                if(strpos($image, '//') == false && strpos($image, '//') !== 0){
                
                    $image = $url."/".$image;
                
                }
            }
            
            $desc = $crawler->filter('p');

            if(count($desc) == 0)
            {
                $desc = null;
            }
            else{
                 $desc = $desc->text();
            }
        }
        
        $data = [
            'link' => $url,
            'title' => $title,
            'image' => $image,
            'description' => $desc,
            'embed_url' => null
        ];
        if(strpos($url, 'ted.com') != false){
            $data['embed_url'] = preg_replace('/www/', 'embed-ssl', $url);
        }
        

        return $data;

    }


    public function responseMessage($message, $status_code = 200)
    {
        $response = [
            'data' => $message,
            'http_status' => $status_code
        ];
        return response()->json($response, $status_code);
    }
    public function responseError($message, $status_code = 400)
    {
        $response = [
            'error' => $message,
            'http_status' => $status_code
        ];
        return response()->json($response, $status_code);
    }
}
