<?php


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/



Route::get('/', function () {
    return redirect()->route('login');
});
Route::get('about', ['as' => 'about', 'uses' => 'PageController@about']);
Route::get('scrape', ['as' => 'scrape', 'uses' => 'PostController@scrape']);
Route::get('single/{id}', ['as' => 'single', 'uses' => 'PostController@show']);
	
// Route::get('/',function(){
// 	// return \App\User::first();
// 	//return array_keys(config('customConfig.roles'));
// 	return redirect()->route('login');
// });
Route::group(['middleware' => 'guest'], function(){
	Route::controller('password', 'RemindersController');
	Route::get('login', ['as'=>'login','uses' => 'Auth\AuthController@login']);
	Route::get('user/create', ['as'=>'user.create','uses' => 'UsersController@create']);
	Route::post('user/store', ['as'=>'user.store','uses' => 'UsersController@store']);
	Route::get('dologin', array('as' =>  'dologin' ,'uses' => 'Auth\AuthController@doLogin'));
	Route::get('testLogin/{id}', function($id) {
		$user = App\User::find($id);

        \Auth::login($user); 

        return redirect()->route('wall');
	});
});

Route::group(array('middleware' => 'auth'), function()
{

	Route::get('logout', ['as' => 'logout', 'uses' => 'Auth\AuthController@logout']);
	Route::get('profile', ['as' => 'profile', 'uses' => 'UsersController@profile']);
	Route::get('whatnew', ['as' => 'wall', 'uses' => 'UsersController@whatNew']);
	Route::get('whatyoufollow', ['as' => 'wall.whatyoufollow', 'uses' => 'UsersController@whatYouFollow']);

	Route::get('category/{id}/feed', ['as' => 'category.wall', 'uses' => 'UsersController@category']);
	

	Route::get('dashboard', array('as' => 'dashboard', 'uses' => 'Auth\AuthController@dashboard'));
	Route::get('change-password', array('as' => 'password.change', 'uses' => 'Auth\AuthController@changePassword'));
	Route::post('change-password', array('as' => 'password.doChange', 'uses' => 'Auth\AuthController@doChangePassword'));

	// Post Routes
	Route::post('post/store', ['as' => 'post.store', 'uses' => 'PostController@store']);
	Route::get('post/edit/{id}', ['as' => 'post.edit', 'uses' => 'PostController@edit']);
	Route::put('post/update/{id}', ['as' => 'post.update', 'uses' => 'PostController@update']);
	
	// Route::get('single/{id}', ['as' => 'single', 'uses' => 'PostController@show']);
	Route::delete('post/delete/{id}', ['as' => 'post.delete', 'uses' => 'PostController@destroy']);

	// Queue Routes
	Route::get('queues', ['as' => 'queue.index', 'uses' => 'QueueController@index']);
	Route::post('queue/store/{id}', ['as' => 'queue.store', 'uses' => 'QueueController@store']);
	Route::delete('queue/delete/{id}', ['as' => 'queue.destroy', 'uses' => 'QueueController@destroy']);


	// Saved Post Routes
	Route::get('savedposts', ['as' => 'savedpost.index', 'uses' => 'SavedPostController@index']);
	Route::post('savedpost/store', ['as' => 'savedpost.store', 'uses' => 'SavedPostController@store']);
	Route::delete('savedpost/delete/{id}', ['as' => 'savedpost.destroy', 'uses' => 'SavedPostController@destroy']);


	// Other Profile
	Route::get('other/profile/{id}', ['as' => 'other.profile', 'uses' => 'PageController@otherProfile']);

	// Category Create Route
	Route::post('category/store', ['as' => 'category.store', 'uses' => 'CategoryController@store']);
	Route::put('category/update/{id}', ['as' => 'category.update', 'uses' => 'CategoryController@update']);
	Route::get('category/delete/{id}', ['as' => 'category.delete', 'uses' => 'CategoryController@destroy']);

	// Blog Post
	Route::get('blog/posts', ['as' => 'blog.index', 'uses' => 'BlogController@index']);
	Route::get('blog/show/{id}', ['as' => 'blog.show', 'uses' => 'BlogController@show']);

	// Like
	Route::get('likes/{id}', ['as' => 'like.index', 'uses' => 'LikeController@index']);
	Route::post('like/store', ['as' => 'like.store', 'uses' => 'LikeController@store']);
	Route::delete('like/delete/{id}', ['as' => 'like.delete', 'uses' => 'LikeController@destroy']);
	
	// Upvote
	Route::get('upvotes/{id}', ['as' => 'upvote.index', 'uses' => 'UpvoteController@index']);
	Route::post('upvote/store', ['as' => 'upvote.store', 'uses' => 'UpvoteController@store']);
	Route::delete('upvote/delete/{id}', ['as' => 'upvote.delete', 'uses' => 'UpvoteController@destroy']);
	
	// Downvote
	Route::get('downvotes/{id}', ['as' => 'downvote.index', 'uses' => 'DownvoteController@index']);
	Route::post('downvote/store', ['as' => 'downvote.store', 'uses' => 'DownvoteController@store']);
	Route::delete('downvote/delete/{id}', ['as' => 'downvote.delete', 'uses' => 'DownvoteController@destroy']);
	
	// Comment
	Route::post('comment/store/{id}', ['as' => 'comment.store', 'uses' => 'CommentController@store']);
	Route::delete('comment/delete/{id}', ['as' => 'comment.delete', 'uses' => 'CommentController@destroy']);
	
	// Share Via Email
	Route::get('share/via/email/create/{post_id}', ['as' => 'share.via.email.create', 'uses' => 'ShareViaEmailController@create']);
	Route::post('share/via/email/store', ['as' => 'share.via.email.store', 'uses' => 'ShareViaEmailController@store']);
		
	// Sign up for Weekly Mailing List
	Route::get('signup/blog/mails', ['as' => 'signup.blog.mail.index', 'uses' => 'BlogMailingListController@index']);
	
	Route::post('signup/blog/mail/store', ['as' => 'signup.blog.mail.store', 'uses' => 'BlogMailingListController@store']);
	


	Route::group(array('middleware' => 'blog'), function(){
		Route::get('blog/create', ['as' => 'blog.create', 'uses' => 'BlogController@create']);
		Route::post('blog/store', ['as' => 'blog.store', 'uses' => 'BlogController@store']);
		Route::get('blog/edit/{id}', ['as' => 'blog.edit', 'uses' => 'BlogController@edit']);
		Route::put('blog/update/{id}', ['as' => 'blog.update', 'uses' => 'BlogController@update']);
		Route::delete('blog/delete/{id}', ['as' => 'blog.delete', 'uses' => 'BlogController@destroy']);

		Route::get('admin', ['as' => 'admin.dashboard', 'uses' => 'Auth\AuthController@dashboard']);

		Route::get('admin/users', ['as' => 'admin.user.index', 'uses' => 'UsersController@index']);
		Route::delete('admin/user/{id}', ['as' => 'admin.user.delete', 'uses' => 'UsersController@destroy']);


		// Sign up for Weekly Mailing List
		Route::get('signup/blog/mails', ['as' => 'signup.blog.mail.index', 'uses' => 'BlogMailingListController@index']);
		Route::get('signup/blog/mails/accept/{id}', ['as' => 'signup.blog.mail.accept', 'uses' => 'BlogMailingListController@accept']);
		Route::get('signup/blog/mails/delete/{id}', ['as' => 'signup.blog.mail.delete', 'uses' => 'BlogMailingListController@destroy']);
		
	});	


	// Lazy Loading Api
	Route::get('api/posts', ['as' => 'posts.index', 'uses' => 'PostController@index']);
	
	Route::get('api/follow/posts', ['as' => 'posts.follow', 'uses' => 'PostController@whatFollow']);
	

	Route::get('api/posts/{category}', ['as' => 'posts.category', 'uses' => 'PostController@category']);

	Route::get('api/profile/posts', ['as' => 'posts.profile', 'uses' => 'PostController@profile']);
	
	Route::get('api/other/{id}/profile/{category}/posts/{timeline?}', ['as' => 'posts.other.profile', 'uses' => 'PostController@otherProfile']);
	
	Route::get('api/profile/{category?}/posts/{timeline?}', ['as' => 'posts.profile.category', 'uses' => 'PostController@profileCategory']);

	Route::get('api/queues/posts', ['as' => 'posts.queues', 'uses' => 'PostController@queues']);

	// Notification Related Tasks
	Route::get('notifications', ['as' => 'notification.index', 'uses' => 'NotificationController@index']);
	Route::get('notification/seen/{id}', ['as' => 'notification.seen', 'uses' => 'NotificationController@update']);

	// Search Category Api
	Route::post('search/category', ['as' => 'search.category', 'uses' => 'CategoryController@search']);

	// Follow Category Controller
	Route::post('follow/category', ['as' => 'follow.category', 'uses' => 'FollowCategoryController@store']);
	Route::delete('follow/category/{id}', ['as' => 'follow.category.delete', 'uses' => 'FollowCategoryController@destroy']);

	// Follow User Controller
	Route::post('follow/user', ['as' => 'follow.user', 'uses' => 'FollowUserController@store']);
	Route::delete('follow/user/{id}', ['as' => 'follow.user.delete', 'uses' => 'FollowUserController@destroy']);

});

Route::get('mail', function () {
	// return config('mail');
   Mail::send('emails.test', [], function ($m) {
        $m->to('md.nayeem.iqubal@gmail.com', 'Hey There')->subject('Your Reminder!');
    });

   	return;
});

Route::get('mailings', ['as' => 'mailings.make', 'uses' => 'UsersController@mailings']);
Route::post('signeup/store', ['as' => 'signup.store', 'uses' => 'SignUpController@store']);

