<?php

namespace App\Http\Middleware;

use Closure;
use App\Blog;

class VerifyBlogOwner
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if(!Blog::isBlogOwner()){
            return redirect()->route('wall');
        }

        return $next($request);
    }
}
