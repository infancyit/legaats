<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
class Blog extends Model
{
    //

    public static function isBlogOwner(){
    	$user = Auth::user()->uid;
    	return ($user == '1' || $user == '10154041983815240') ? true : false;
    }


    public static function getSideBar(){
    	$posts = Blog::orderBy('id', 'desc')->get();
    	// foreach ($posts as $post) {
    	// 	# code...
    	// }
    	return $posts;
    }
}
