<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        foreach (range(1, 5) as $index) {
            $user = new User;
            $user->uid = $index;
            $user->fullname = $faker->name ;
            $user->email = $faker->email;
            $user->avatar = 'images/po.jpg';
            $user->fb_access_token = $faker->name;

            $user->save();
        }
        

    }
}
