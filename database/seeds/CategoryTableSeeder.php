<?php

use Illuminate\Database\Seeder;
use App\Category;
class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories  = [
            [
                'Love',
                'love',
                1
            ], 
            [
                'Death',
                'death',
                2
            ],
            [
                'Entrepreneurship',
                'entrepreneurship',
                3
            ], 
            [
                'Business',
                'business',
                4
            ], 
            [
                'Education',
                'education',
                5
            ],  
        ];
        foreach ($categories as $category) {
        	$persist_category = new Category;
        	$persist_category->name = $category[0];
            $persist_category->url = $category[1];
            $persist_category->order = $category[2];
            
        	$persist_category->save();
        		
        }
    }
}
