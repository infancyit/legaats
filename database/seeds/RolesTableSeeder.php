<?php

use App\Role;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = config('customConfig.roles');
        foreach ($roles as $role => $name) {
            Role::create(['name'=>$role, 'display_name' => $name]);
        }

    }
}
