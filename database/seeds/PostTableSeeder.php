<?php

use Illuminate\Database\Seeder;
use App\Post;
class PostTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        foreach (range(1, 10) as $index) {
        	$post = new Post;
        	$post->user_id = 1;
        	$post->text = $faker->paragraph;
        	$post->save();	
        }
    }
}
