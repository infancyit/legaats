<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSavedPostCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('saved_post_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('saved_post_id')->unsigned();
            $table->foreign('saved_post_id')->references('id')->on('saved_posts')
                ->onUpdate('cascade')->onDelete('cascade');
            

            $table->integer('category_id')->unsigned();
            $table->foreign('category_id')->references('id')->on('categories')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('saved_post_categories');
    }
}
