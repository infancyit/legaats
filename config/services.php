<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, Mandrill, and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => 'sandbox016dc263f2c94f2aa41d1a39a5aa2cc0.mailgun.org',
        'secret' => 'key-2d1c841cfebc543b1e3dbdf86accfc01',
    ],

    'mandrill' => [
        'secret' => env('MANDRILL_SECRET'),
    ],

    'ses' => [
        'key'    => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'stripe' => [
        'model'  => App\Cashier::class,
        'key'    => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'facebook' => [
        'client_id' => '286508528363236',
        'client_secret' => 'a3ad7a43c7188f34a210ead62555c84d',
        'redirect' => PHP_SAPI === 'cli' ? false : asset('/').'user/create',
    ],
];
