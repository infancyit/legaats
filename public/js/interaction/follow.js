$(document).ready(function() {

	function performFollowCategoryAction(followButton, response){
		followButton.removeClass('follow-category-btn').removeClass('btn-success');
		followButton.addClass('btn-danger').addClass('unfollow-category-btn');
		followButton.data('follow-category-id',response.data.id);
		followButton.html('Unfollow');


	}

	function performUnfollowCategoryAction(unfollowButton){
		unfollowButton.removeClass('btn-danger').removeClass('unfollow-category-btn');
		unfollowButton.addClass('follow-category-btn').addClass('btn-success');
		unfollowButton.data('follow-category-id','');
		unfollowButton.html('Follow');
	}

	function performFollowUserAction(followButton, response){
		followButton.removeClass('follow-user-btn');
		followButton.addClass('unfollow-user-btn');
		followButton.data('follow-user-id',response.data.id);
		followButton.html('Unfollow');


	}

	function performUnfollowUserAction(unfollowButton){
		unfollowButton.removeClass('unfollow-user-btn');
		unfollowButton.addClass('follow-user-btn');
		unfollowButton.data('follow-user-id','');
		unfollowButton.html('Follow');
	}

	$(document).on('click', '.follow-category-btn', function(){

		var category_id = $(this).data('category-id');
		followCategoryStoreUrl = baseURL+"follow/category";
		$.ajax({
			url : followCategoryStoreUrl,
			data : { category_id : category_id },
			type : 'POST',
			dataType : 'json',
			followButton : $(this),
			success : function(response){
				performFollowCategoryAction(this.followButton, response);
				console.log(response);
				// console.log('Like');
			},
			error : function(response){
				// console.log(response);
				// console.log('Unlike');
			}

		});
		return false;
	
	});

	$(document).on('click', '.unfollow-category-btn', function(){
		
		var followCatgeoryId = $(this).data('follow-category-id');
		followCategoryDeleteUrl = baseURL+"follow/category/"+followCatgeoryId;
		$.ajax({
			url : followCategoryDeleteUrl,
			data : {},
			type : 'DELETE',
			dataType : 'json',
			unfollowButton : $(this),
			success : function(response){
				performUnfollowCategoryAction(this.unfollowButton);
			},
			error : function(response){
				// console.log(response);
			}

		});
		
		return false;
	
	});


	$(document).on('click', '.follow-user-btn', function(){

		var user_id = $(this).data('user-id');
		followUserStoreUrl = baseURL+"follow/user";
		$.ajax({
			url : followUserStoreUrl,
			data : { user_id : user_id },
			type : 'POST',
			dataType : 'json',
			followButton : $(this),
			success : function(response){
				performFollowUserAction(this.followButton, response);
				console.log(response);
				// console.log('Like');
			},
			error : function(response){
				// console.log(response);
				// console.log('Unlike');
			}

		});
		return false;
	
	});

	$(document).on('click', '.unfollow-user-btn', function(){
		
		var followUserId = $(this).data('follow-user-id');
		followUserDeleteUrl = baseURL+"follow/user/"+followUserId;
		$.ajax({
			url : followUserDeleteUrl,
			data : {},
			type : 'DELETE',
			dataType : 'json',
			unfollowButton : $(this),
			success : function(response){
				performUnfollowUserAction(this.unfollowButton);
			},
			error : function(response){
				// console.log(response);
			}

		});
		
		return false;
	
	});
});