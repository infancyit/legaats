$(document).ready(function() {

	function performLikeAction(likeButton, response){
		likeButton.toggleClass('like-btn').toggleClass('liked-btn');
		likeButton.data('like-id',response.data.id);
		likeButton.html('Unlike');
		var currentLikeCountHolder = likeButton.parent().parent().find('.all-counts').find('a').eq(0);
		var currentLikeCount = +currentLikeCountHolder.data('count');
		currentLikeCountHolder.data('count', (currentLikeCount+1));
		currentLikeCountHolder.html((currentLikeCount+1) + " likes");	
	}

	function performUnlikeAction(likeButton){
		likeButton.toggleClass('liked-btn').toggleClass('like-btn');
		likeButton.html('Like');
		var currentLikeCountHolder = likeButton.parent().parent().find('.all-counts').find('a').eq(0);
		var currentLikeCount = +currentLikeCountHolder.data('count');
		currentLikeCountHolder.data('count', (currentLikeCount-1));
		currentLikeCountHolder.html((currentLikeCount-1) + " likes");
	}
	$(document).on('click', '.like-btn', function(){
		var post_id = $(this).data('post-id');
		$.ajax({
			url : likeStoreUrl,
			data : { post_id : post_id },
			type : 'POST',
			dataType : 'json',
			likeButton : $(this),
			success : function(response){
				performLikeAction(this.likeButton, response);
				// console.log(response);
				// console.log('Like');
			},
			error : function(response){
				// console.log(response);
				// console.log('Unlike');
			}

		});
		return false;
	
	});

	$(document).on('click', '.liked-btn', function(){
		
		var like_id = $(this).data('like-id');
		$.ajax({
			url : likeDeleteUrl+"/"+like_id,
			data : {},
			type : 'DELETE',
			dataType : 'json',
			likeButton : $(this),
			success : function(response){
				performUnlikeAction(this.likeButton);
			},
			error : function(response){
				// console.log(response);
			}

		});
		
		return false;
	
	});

	$(document).on('click','.all-counts a:nth-child(1)',function(){
		
		var post_id = $(this).data('post-id');
		$.ajax({
			url : likeAll+"/"+post_id,
			data : {},
			type : 'GET',
			dataType : 'json',
			button : $(this),
			success : function(response){
				var popover = this.button.data('bs.popover');
				popover.options.content = response.data;
				this.button.popover('show');
			},
			error : function(response){
				console.log();
			}

		});
		
		return false;
	
	});
});