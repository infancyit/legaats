$(document).ready(function() {

	function makeComment(response){
		var placeHolder = $(".post_"+response.data.post_id).find('.comment').last();

		var maker = '<p>'+response.data.text+'</p>'; 
		maker = '<a href="#">'+response.data.user.fullname+'</a><br><br>' + maker;
		maker +=  '<span class="light-text"><a href="#">'+response.data.created+'</a></span>';

		maker = '<div class="btn-group pull-right">\
						<button type="button"class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">\
							<span class="caret"></span>\
						</button>\
						<ul class="dropdown-menu">\
							<li><a class="comment-delete-button" data-toggle="modal"\
								data-target="#deleteCommentModal" data-comment-id="'+response.data.id+'" href="#">Delete</a></li>\
						</ul>\
				</div>'+maker;


		maker = '<div class="col-md-10">'+maker+'</div>';

		maker = '<div class="col-md-2"><img src="'+response.data.user.avatar+'" alt="" class="img-circle" align="center" width="60" height="60"/></div>'+maker;

		maker = '<div class="comment single_comment_'+response.data.id+'"><div class="row">'+maker+'</div></div>';

		placeHolder.before(maker);

		console.log(placeHolder.attr('class'));
	
	}
	$(document).on('submit','.comment-form',function(e){
		var commentStoreUrl = $(this).attr('action');
		// console.log(commentStoreUrl);
		var formData = $(this).serialize();

		// console.log(formData);
		
		$.ajax({
			url : commentStoreUrl,
			data : formData,
			type : 'POST',
			dataType : 'json',
			success : function(response){
				// console.log(response);
				makeComment(response);
			},
			error : function(response){
				// console.log(response);
			}
		});
		$(this).trigger("reset");
		return false;
	});

	var commentholder = null;
	var commentId = null;
	$(document).on('click', '.comment-delete-button', function(){
		commentId = $(this).data('comment-id');
		commentholder = $('.single_comment_'+commentId);

	});

	$('#deleteCommentModal').on('click', '.delete-comment-confirm-button', function(){
		// alert('asd');
		$.ajax({
			url : commentDeleteUrl+"/"+commentId,
			data : {},
			type : 'DELETE',
			dataType : 'json',
			success : function(response){
				commentholder.fadeOut(200);
				$('#deleteCommentModal').modal('toggle');
				
			},
			error : function(response){
				// console.log(response);
			}
		});
		return false;
	});	
});