$(document).ready(function() {
	var currentPageNotification = 0;
	
	function makeNotification(response){
		currentPage = response.data.meta.current_page;
		lastPage = response.data.meta.last_page;
		notifications = response.data.content;
		console.log(notifications);
		console.log(currentPage);
		console.log(lastPage);
		notificationContent = "";

		for (var i = 0; i < notifications.length; i++) {

			if(notifications[i].type == 'like'){
				notificationContent += '<li class="well '+notifications[i].status+'" data-notification-id="'+notifications[i].id+'">'+
						                    '<a target="_blank" href="'+baseURL+'single/'+notifications[i].post_id+'">'+
						                      '<b>'+notifications[i].user_who.fullname+'</b>  likes your <span style="color:blue;">post</span><br>'+
						                      '<span class="light-text">'+notifications[i].timeline+'</span>'+
						                    '</a>'+
						                '</li>';
			}
			else
			{
				notificationContent += '<li class="well '+notifications[i].status+'" data-notification-id="'+notifications[i].id+'">'+

						                    '<a target="_blank" href="'+baseURL+'single/'+notifications[i].post_id+'">'+
						                      '<b>'+notifications[i].user_who.fullname+'</b> commented on your <span style="color:blue;">post</span><br>'+
						                      '<span class="light-text">'+notifications[i].timeline+'</span>'+
						                    '</a>'+

						                '</li>';
			}

			

		};
		$('.notificaion-content').before(notificationContent);
		if(currentPage >= lastPage){
			$('.notificaion-content').hide();
			return;
		}



		currentPageNotification = currentPage;

	}

	function processShowMoreNotification(){
		currentPageNotification += 1;
		$.ajax({
			url : baseURL+"notifications?page="+currentPageNotification,
			type : 'GET',
			dataType : 'json',
			success : function(response){
				makeNotification(response);
			},
			error : function(response){
				// console.log(response);
			}
		});
	}
	$(document).on('click','.show-more-notification',function(e){
		processShowMoreNotification();
		return false;
	});
	processShowMoreNotification();
});

