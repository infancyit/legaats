@extends('layouts.default')
    @section('content')
        @include('includes.alert')
        <?php 
            use App\PostCategory;
         ?>
        <div class="row">
            <div class="col-md-8 main">
                    <!-- Edit Post Section -->
                    <article class="br2 ba  b--black-10 mv8 w-100 w-50-m  mw0 center">
                        <div style="padding:1.5rem;">
                            <div class="dtc v-mid">
                                <img src="{{$user->avatar}}" alt="" class="img-circle" width="60" height="60" />
                            </div>
                            <div class="dtc v-mid pl3">
                                <p class="lh-copy">
                                    <a href="{{route('profile')}}">{{$user->fullname}}</a> 
                                </p>
                            </div>

                            <img class="pull-right" id="loading-icon"  src="{{asset('images/loading.gif')}}" alt="" width="60" height="60" />
                            <br>
                            {!! Form::open(array('route' =>  ['post.update', $post->id] ,'id' => 'post-form', 'files' => true ,'method' => 'put')) !!}
                            <textarea name="text" class="form-control post" placeholder="Whats on your mind today ?" required>{{$post->text}}</textarea>
                              
                            <input type="file" name="photo" id="photo"  style="display:none;" onchange="readURL(this);" />
                            <div id="image-preview">
                                <img src="{{($post->image == null) ? '' : asset($post->image->url)}}" id="image-preview-element" style="width:100%;" class="img-responsive"><br>
                            </div>
                            
                            <div id="preview">
                                @if($post->scrape)
                                    @include('sections.scrape')
                                @endif
                            </div>
                            </br>
                            
                            <a href="#" id="photo-file-btn"><span class="glyphicon glyphicon-camera glyphicon-camera-selected"></span></a>
                            <select name="category_id[]" id="example-getting-started" multiple="multiple">
                                <?php 
                                    $categories = App\Category::all();
                                 ?>
                           
                                @foreach($categories as $category)

                                    <?php 
                                        $postCategory = PostCategory::wherePostId($post->id)->whereCategoryId($category->id)->first();
                                     ?>
                                    <option value="{{$category->id}}" {{ ($postCategory == null) ? '' : 'selected'}}>{{$category->name}}</option>
                                @endforeach
                            </select>


                            <input type="submit" class="btn my-btn btn-default" style="float:right;" value="Post"/>
                            {!! Form::close() !!}
                        </div>
                    </article>
                    <br>
                </div>
                <div class="col-md-4">
                    <div class="row">
                        <div class="col-md-12">
                            <article class="br2 ba  b--black-10 mv8 w-100 w-50-m  mw0 center pa3 pa4-ns">
                              <div class="tc">
                                <img src="{{$user->avatar}}" alt="" class="img-circle" width="60" class="br-100 h4 w4 dib ba b--black-05 pa2" title="Kitty staring at you">
                                <h1 class="f3 mb2">Full Name : {{$user->fullname}}</h1>
                                <h2 class="f4 fw4 gray mt0">Email : {{$user->email}}</h2>
                              </div>
                            </article>

                            <br>

                                                                   
                        </div>
                    </div>
                </div>
             
            </div>
        


         <!-- page end-->
              <!-- Modal -->
                @include('sections.modals')
                
                
@stop

@section('script')
    <script type="text/javascript">
        $(document).ready(function(){
            var baseURL = "{{asset("/")}}";
            var saveButtonReference = null;
            $('#photo-file-btn').click(function(){
                $('#photo').trigger('click');
                console.log('CLick');
                return false;
            });
            $('#photo').change(function(){
                var fileName = $(this).val();
                $('.image-preview').html('File Selected');
            });


           
            $('.save-button').click(function(){
                var postId = $(this).data('post-id');
                $('.save-changes-btn').data('post-id', postId);
                saveButtonReference = $(this);
            });

            // $('.post-modal-toggle-btn').click(function(){
            //     if($('textarea[name=\'text\']').val() != ''){
            //         // alert('asdas');
            //        $('#createModal').modal('toggle');
            //     }

            //     return false;
            // });
            
          
            // $('.post-btn').click(function(){
            //     $('#post-form').trigger('submit');
            //     return false;

            // });
            
            $('.discard-button').click(function(){

                    var postId = $(this).data('post-id');
                    
                    var requestUrl = baseURL+"savedpost/delete/"+postId;
         
                    // Ajax Call
                    $.ajax({
                        url: requestUrl, 
                        data :{
                        },
                        method : 'Delete',
                        dataType : 'json',
                        post : $(this).parent().parent(),
                        success: function(response){
                           
                            // var message = response.data.message;
                            // var success = '<div class="alert alert-success alert-dismissable fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+message+'</div>'
                            // this.span.html('');
                            console.log('success');
                            this.post.hide(500);
                        
                        },
                        error: function(errorResponse){
                           
                            // var message = "";
                            // var messages = jQuery.parseJSON(errorResponse.responseText);
                            // console.log(messages);
                            // if(messages.error.http_status == 400){
                            //      $.each(messages.error.message, function(i, item) {
                            //         message += item+"<br>";
                            //     })
                            // }
                            // else
                            // {
                            //     message = messages.error.message;
                            // }
                           
                            // var error = '<div class="alert alert-danger alert-dismissable fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+message+'</div>';
                            // this.span.html('');
                            // this.span.append(error);
                        }
                    });

                
                    // Ajax Call End 
                    return false;

            });

        });
        
    </script>
@stop