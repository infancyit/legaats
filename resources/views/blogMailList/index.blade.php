@extends('layouts.default')
    @section('content')
        		
	        <?php use Carbon\Carbon; ?>
	     	<div class="row">
				<div class="col-md-8 main">
				<div class="posts-section">
						@include('includes.alert')
						<h2>Total Users : {{$total_user_count}}</h2>
						<h2>All Mailings</h2>
						<table id="example" class="display" cellspacing="0" width="100%">
					        <thead>
					            <tr>
					                <th>User ID</th>
					                <th>Full Name</th>
					                <th>Email</th>
					                <th>Status</th>
					                <th>Actions</th>
					                <!-- <th>Salary</th> -->
					            </tr>
					        </thead>
					        <tbody>
					        	@foreach($mailings as $mailing)
					            <tr>
					                <td>{{$mailing->user_id}}</td>
					                <td>{{$mailing->user->fullname}}</td>
					                <td>{{$mailing->email}}</td>
					                <td>{{$mailing->status}}</td>
					                <td>
					                	@if($mailing->status != 'Accepted')
					                	<a class="btn btn-primary" href="{{route('signup.blog.mail.accept', $mailing->id)}}">Accept</a>
					                	
					                	<!-- <a class="btn btn-primary" href="{{route('signup.blog.mail.accept', $mailing->id)}}">Pending</a> -->
					                	@endif
					                	<a class="btn btn-danger" href="{{route('signup.blog.mail.delete', $mailing->id)}}">Delete</a>

					                </td>
					            </tr>
					    		@endforeach
					        </tbody>
					    </table>
					    <h2>All Accepted Mails (,) Separated</h2>
					    <div class="well">

					    		{{implode(' , ', App\BlogMailingList::whereStatus('Accepted')->lists('email')->toArray())}}
					    
					    </div>		
				</div>
				<div class="posts-footer text-center">
					<img id="posts-loading-icon"  src="{{asset('images/loading.gif')}}" alt="" width="60" height="60" />
					<!-- <a class="btn show-more-btn">Show More</a> -->
				</div>
				
				</div>
				<div class="col-md-4">
					<div class="row">
						<div class="col-md-12">
							<div class="right"> 

								@include('sections.sidebarForProfile')  
							</div>
						</div>
					</div>
				</div>
			</div>
                
	      

              <!-- page end-->
              <!-- Modal -->
			@include('sections.modals')
@stop


@section('script')
	</script>
	<script type="text/javascript">
        
        $(document).ready(function(){
            currentUrl = baseURL + "/api/profile/posts";
            $('.edit-category-form').hide();
            
            $('#example').DataTable( {
		        "order": [[ 1, "asc" ]]
		    });	
		    $('#posts-loading-icon').hide();
		    stopLoading = true;

             

            $(document).on('click', '.discard-button', function(){
                    var savedPostId = $(this).data('save-post-id');
                    articleReference = $(".post_"+$(this).data('post-id'));
                    var requestUrl = baseURL+"savedpost/delete/"+savedPostId;
                    // Ajax Call
                    $.ajax({
                        url: requestUrl, 
                        data :{
                        },
                        method : 'Delete',
                        dataType : 'json',
                        success: function(response){
                            articleReference.hide(500);                
                        },
                        error: function(errorResponse){
                           
                        }
                    });
                    // Ajax Call End 
                    return false;
            });

         


            // Edit Category
            var edit_category_id = null;
            var edit_form = $('.edit-category-form');

            $('.edit_category_btn').click(function(){
                edit_category_id = $(this).parent().data('category-id');

                var name = $(this).parent().parent().find('a').eq(0).html();
                edit_form.find('input[name="name"]').val(name);
                $('.edit-category-form').fadeIn(200);
                return false;
            });

            $('.edit-category-form').submit(function(){

                var requestUrl = $(this).attr('action')+"/"+edit_category_id;
                var formData = $(this).find("input").serializeArray();
                // console.log(formData);
                // Ajax Call
                    $.ajax({
                        url: requestUrl, 
                        data : formData,
                        method : 'PUT',
                        dataType : 'json',
                        span : $(this).parent().find('.error'),
                        success: function(response){
                           
                            // var message = response.data.message;
                            // var success = '<div class="alert alert-success alert-dismissable fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+message+'</div>'
                            // this.span.html('');
                            console.log('success');
                            location.reload();
                        
                        },
                        error: function(errorResponse){
                           
                            var message = "";
                            var messages = jQuery.parseJSON(errorResponse.responseText);
                            console.log(messages);
                            if(messages.http_status == 400){
                                 $.each(messages.error, function(i, item) {
                                    message += item+"<br>";
                                })
                            }
                            else
                            {
                                message = messages.error;
                            }
                           
                            var error = '<div class="alert alert-danger alert-dismissable fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+message+'</div>';
                            this.span.html('');
                            this.span.append(error);
                        }
                    });

                
                    // Ajax Call End

                return false;
            });
            
            // Profile Timeline Related Codes

            $('#select-category-profile').change(function(){
                currentUrl = baseURL+"api/profile/"+$(this).val()+"/posts";
                
                clearPostSection();
                showMorePosts();
            });

            $('.timeline-select').click(function(){

                timeline = $(this).data('timeline');
                currentUrl = baseURL+"api/profile/"+$('#select-category-profile').val()+"/posts/"+timeline;

                console.log(currentUrl);
                clearPostSection();
                showMorePosts();
                return false;
            });
            
        });
        
    </script>
@stop