@extends('layouts.default')
    @section('content')
        		
	        <?php use Carbon\Carbon; ?>
	     	<div class="row">
				<div class="col-md-8 main">
					<!-- <div class="order-by-container">
                        
                          <button type="button" class="btn btn-default order-by-btn" data-order-by="like">By Likes</button>
                          <button type="button" class="btn btn-default order-by-btn" data-order-by="comment">By Comments</button>
                          <button type="button" class="btn btn-default order-by-btn" data-order-by="upvote">By Upvotes</button>
                          <button type="button" class="btn btn-default order-by-btn" data-order-by="downvote">By Downvotes</button>
                        
                    </div> 
                    <br>-->
					<div class="row">
						<div class="col-md-12">
							<!-- Start Cart content -->
							<div class="posts-section">
								
							</div>
							<div class="posts-footer text-center">
								<img id="posts-loading-icon"  src="{{asset('images/loading.gif')}}" alt="" width="60" height="60" />

								<!-- <a class="btn show-more-btn">Show More</a> -->
							</div>
							
							<!-- End Card Content -->

						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="row">
						<div class="col-md-12">
							<div class="right">

								@include('includes.rightSideBar')
							</div>
						</div>
					</div>
				</div>
			</div>
                
	      

              <!-- page end-->
              <!-- Modal -->
				@include('sections.modals')
				
@stop


@section('script')

	<script type="text/javascript">
	    $(document).ready(function(){
	    
	    	
	    	var saveButtonReference = null;
	    	var articleReference = null;
	    	var article = null;

	   		currentUrl = baseURL + "api/queues/posts";

	        $('#photo-file-btn').click(function(){
	            $('#photo').trigger('click');
	            console.log('CLick');
	            return false;
	        });
	        $('#photo').change(function(){
	            var fileName = $(this).val();
	            $('.image-preview').html('File Selected');
	        });
	        showMorePosts();

	        $('#post-btn').click(function(){
	            $('#post-form').trigger('submit');
	            return false;
	        });

	        $(document).on('click', '.save-button', function(){
	        	saveButtonReference = $(this);
	        	articleReference = $(".post_"+saveButtonReference.data('post-id'));   	
	        });

	        $(document).on('click', '.save-changes-btn', function(){

	        		var postId = saveButtonReference.data('post-id');
	        		var categories = new Array();//$("input[name='category_id[]']:checked").serializeArray();
	        		var queue_id = saveButtonReference.data('queue-id');
	        		
	        		
	        		// console.log(postId);
	        		// console.log($('.category-input').val());
	        		$("input[name='category_id[]']:checked").each(function(i) {
						categories.push($(this).val());
					});
					if(categories.length > 3){
						var message = "You can select at most 3 Categories";
			            var success = '<div class="alert alert-danger alert-dismissable fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+message+'</div>'
			            $('#error').html(success);
						return;
					}
	        		var requestUrl = baseURL+"savedpost/store";
	        		// categories =  JSON.stringify(categories);
	        		// console.log(categories);
	        		// return;
	        		// return;
	        		// Ajax Call
	        		$.ajax({
			            url: requestUrl, 
			            data :{
			            	post_id : postId,
			            	category_id : categories,
			            	queue_id : queue_id
			            },
			            method : 'POST',
			            dataType : 'json',
			            success: function(response){
			               
			                // var message = response.data.message;
			                // var success = '<div class="alert alert-success alert-dismissable fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+message+'</div>'
			                // this.span.html('');
			                // console.log(response);
			                // return;
			                
			  
			                articleReference.hide(500);
			                
			                $('#myModal').modal('toggle');
			                $("input[name='category_id[]']").attr('checked', false);
			                $('#error').html('');
			            
			            },
			            error: function(errorResponse){
			               	console.log(errorResponse);
			               
			            }
			        });
					// Ajax Call End 

					return false;

	        });

			$(document).on('click', '.unqueue-button', function(){
	        		var postId = $(this).data('queue-id');
	        	
	        		articleReference = $(this).parent().parent();
	        		var requestUrl = baseURL+"queue/delete/"+postId;
	     
	        		// Ajax Call
	        		$.ajax({
			            url: requestUrl, 
			            data :{
			            },
			            method : 'Delete',
			            success: function(response){
			               
			                // var message = response.data.message;
			                // var success = '<div class="alert alert-success alert-dismissable fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+message+'</div>'
			                // this.span.html('');

			                console.log('success');
			               	articleReference.hide(500);
			            
			            },
			            error: function(errorResponse){
			               
			                // var message = "";
			                // var messages = jQuery.parseJSON(errorResponse.responseText);
			                // console.log(messages);
			                // if(messages.error.http_status == 400){
			                //      $.each(messages.error.message, function(i, item) {
			                //         message += item+"<br>";
			                //     })
			                // }
			                // else
			                // {
			                //     message = messages.error.message;
			                // }
			               
			                // var error = '<div class="alert alert-danger alert-dismissable fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+message+'</div>';
			                // this.span.html('');
			                // this.span.append(error);
			            }
			        });

				
					// Ajax Call End 
					return false;

	        });


	    });
	    
	</script>
@stop