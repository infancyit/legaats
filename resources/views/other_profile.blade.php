@extends('layouts.default')
    @section('content')
        @include('includes.alert')
        <div class="row">
            <div class="col-md-8 main">
                    
                    
                    <div class="row">
                        <div class="col-md-12">
                            <!-- Start Cart content -->

                            <div class="posts-section">
                                
                            </div>
                            <div class="posts-footer text-center">
                                <img id="posts-loading-icon"  src="{{asset('images/loading.gif')}}" alt="" width="60" height="60" />
                                <!-- <a class="btn show-more-btn">Show More</a> -->
                            </div>
                            <!-- End Card Content -->

                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="row">
                        <div class="col-md-12">
                            <article class="br2 ba  b--black-10 mv8 w-100 w-50-m  mw0 center pa3 pa4-ns">
                              <div class="tc">
                                <img src="{{$user->avatar}}" alt="" class="img-circle" width="80" height="80" class="br-100 h4 w4 dib ba b--black-05 pa2" title="Kitty staring at you">
                                <h1 class="f3 mb2">Full Name : {{$user->fullname}}</h1>
                                <h2 class="f4 fw4 gray mt0">Email : {{$user->email}}</h2>
                              </div>
                            </article>

                            <br>
                            <br> 
                            <?php 
                                $categories = App\Category::all();
                            ?>
                            Category : <select id="select-category-profile">
                                    <option value="0">All</option>
                               
                              @foreach($categories as $category)
                                  <option value="{{$category->id}}">{{$category->name}}</option>
                              @endforeach
                            </select>
                            <br>
                            <br>
                            <!-- Timeline -->
                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                              <?php 
                                    $year = (int) date('Y');
                                    $months = [
                                        'January',
                                        'February',
                                        'March',
                                        'April',
                                        'May',
                                        'June',
                                        'July',
                                        'August',
                                        'September',
                                        'October',
                                        'November',
                                        'December'
                                    ];
                               ?>

                               @foreach($timelines as $timeline)
                              <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingOne">
                                  <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#year_{{$timeline->year}}" aria-expanded="false" aria-controls="year_{{$timeline->year}}">
                                      {{$timeline->year}}
                                    </a>
                                  </h4>
                                </div>
                                <div id="year_{{$timeline->year}}" class="panel-collapse collapse" role="tabpanel">
                                    <div class="panel-body">
                                        <ul class="nav nav-pills nav-stacked">
                                            @foreach($timeline->months as $month)
                                                <li role="presentation"><a href="#" class="timeline-select" data-timeline="{{sprintf("%d-%02d",$timeline->year,$month)}}">{{$months[$month-1]}}</a></li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                              </div>
                              @endforeach
                            </div>
                                         
                        </div>
                    </div>
                </div>
             
            </div>
        


         <!-- page end-->
              <!-- Modal -->
        @include('sections.modals')
                
                
@stop

@section('script')
    <script type="text/javascript">
        $(document).ready(function(){
            var saveButtonReference = null;
            currentUrl = baseURL + "api/other/"+"{{$user->id}}"+"/profile/0/posts";
            showMorePosts();


           
            $(document).on('click', '.save-button', function(){
                var postId = $(this).data('post-id');
                $('.save-changes-btn').data('post-id', postId);
                saveButtonReference = $(this);
            });

            $(document).on('click', '.save-changes-btn', function(){
                    var postId = $(this).data('post-id');
                    var categories = new Array();//$("input[name='category_id[]']:checked").serializeArray();
                    // console.log(postId);
                    // console.log($('.category-input').val());
                    $("input[name='category_id[]']:checked").each(function(i) {
                        categories.push($(this).val());
                    });
                    if(categories.length > 3){
                        var message = "You can select at most 3 Categories";
                        var success = '<div class="alert alert-danger alert-dismissable fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+message+'</div>'
                        $('#error').html(success);
                        return;
                    }
                    var requestUrl = baseURL+"savedpost/store";
                    // categories =  JSON.stringify(categories);
                    // console.log(categories);
                    // return;
                    // return;
                    // Ajax Call
                    $.ajax({
                        url: requestUrl, 
                        data :{
                            post_id : postId,
                            category_id : categories
                        },
                        method : 'POST',
                        dataType : 'json',
                        success: function(response){
                           
                            // var message = response.data.message;
                            // var success = '<div class="alert alert-success alert-dismissable fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+message+'</div>'
                            // this.span.html('');
                            // console.log(response);
                            // return;
                            
                            saveButtonReference.hide(500);
                            $('#myModal').modal('toggle');
                            $("input[name='category_id[]']").attr('checked', false);
                            $('#error').html('');
                        
                        },
                        error: function(errorResponse){
                           
                            var messages = jQuery.parseJSON(errorResponse.responseText);
                            console.log(messages);
                          
                        }
                    });
                    // Ajax Call End 

                    return false;

            });

            $(document).on('click', '.queue-button', function(){
                    var postId = $(this).data('post-id');
                    
                    var requestUrl = baseURL+"queue/store/"+postId;
         
                    // Ajax Call
                    $.ajax({
                        url: requestUrl, 
                        data :{
                        },
                        method : 'POST',
                        dataType : 'json',
                        queueButton : $(this),
                        success: function(response){
                           
                            console.log('success');
                            this.queueButton.hide(500);
                        
                        },
                        error: function(errorResponse){
                        
                        }
                    });

                
                    // Ajax Call End 
                    return false;

            });

            $('#select-category-profile').change(function(){
                currentUrl = baseURL+"api/other/"+"{{$user->id}}"+"/profile/"+$(this).val()+"/posts";
                
                clearPostSection();
                showMorePosts();
            });

            $('.timeline-select').click(function(){

                timeline = $(this).data('timeline');
                // currentUrl = baseURL+"api/profile/"+$('#select-category-profile').val()+"/posts/"+timeline;
                currentUrl = baseURL+"api/other/"+"{{$user->id}}"+"/profile/"+$('#select-category-profile').val()+"/posts/"+timeline;


                console.log(currentUrl);
                clearPostSection();
                showMorePosts();
                return false;
            });
          
            
            
        });
        
    </script>
@stop