{!! Form::open(['url' => route('share.via.email.store') ,'id' => 'shareViaEmailForm', 'files' => true]) !!}
  {!! Form::hidden('post_id', $post->id) !!}

  <div class="form-group">
    <label for="email">Email Address</label>
   {!! Form::email('email', $value = null, ['class' => 'form-control', 'placeholder' => 'Email Address', 'required']) !!}
   
  </div>
  <div class="form-group">
    <label for="Subject">Subject</label>
    <p class="form-control">{{str_limit($post->text, 70)}}</p>
  </div>
  <div class="form-group">
    <label for="Body">Body</label>
    <p class="form-control">
      {{Auth::user()->fullname}} shared with you a post want to see <a target="_blank" href="{{route('single', $post->id)}}">Click Here</a> 
    </p> 
  </div>
  <button type="submit" class="btn btn-default">Send</button>
  {!! Form::close() !!}