<!-- right sidebar -->

 
  <?php

    $sides = App\Post::getSideBarInformation(); 

  ?>
  @foreach($sides as $side)
    @if(count($side->posts))
  <div class="">
    <div style="padding-left:4rem;">
      <h5 class="light-text">{{$side->name}}</h5>
      <!-- <h5>Beyond the headlines</h5> -->
    </div>
    <hr>
    <ul>
      @foreach($side->posts as $post)
      <li class="custom-list">
        <div class="dtc v-mid">
            <img src="{{$post->user->avatar}}" alt="" class="img-circle" width="60" height="60" />
        </div>
        <div class="dtc v-mid pl3">
            <p class="lh-copy">
              <a href="#">{{$post->user->fullname}}</a> <!-- <span class="light-text">in</span> <a href="#">FuckHard18</a> -->
              <br><span class="light-text">{{str_limit($post->text, 40)}}</span>
              <br><span class="light-text">{{Carbon::parse($post->created_at)->diffForHumans()}}</span>
            </p>
        </div>
      </li>
      @endforeach
    </ul>
  </div>

  <!-- end right sidebar -->
  <br>
  @endif
  @endforeach