<footer class="pv4 ph3 ph5-m ph6-l mid-gray">
    <small class="f6 db tc">© {{date('Y')}}  <b class="">Legaats </b>., All Rights Reserved</small>
    <div class="tc mt3">
      <a href="#" title="Language" class="f6 dib ph2 link mid-gray dim">Language</a>
      <a href="#"    title="Terms" class="f6 dib ph2 link mid-gray dim">Terms of Use</a>
      <a href="#"  title="Privacy" class="f6 dib ph2 link mid-gray dim">Privacy</a>
    </div>
  </footer>

  <!-- end navbar -->


<!-- javascript-->
  <script src="{{asset('js/jquery-2.1.1.min.js')}}"></script>
  <script src="{{asset('js/jquery-ui.min.js')}}"></script>
  
  <script src="{{asset('js/bootstrap.min.js')}}"></script>
  <script src="{{asset('js/bootstrap-multiselect.js')}}"></script>

  <!-- Include Interaction -->
  <script src="{{asset('js/interaction/like.js')}}"></script>
  <script src="{{asset('js/interaction/follow.js')}}"></script>
  <script src="{{asset('js/interaction/comment.js')}}"></script>
  <script src="{{asset('js/notification.js')}}"></script>

  <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
  
  
  

  
  <script type="text/javascript">
    // Url Parsing Algorithm
    function getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }
    
    var likeAll = "{{route('like.index', false)}}";
    var likeStoreUrl = "{{route('like.store')}}";
    var likeDeleteUrl = "{{route('like.delete', false)}}";

    var upvoteAll = "{{route('upvote.index', false)}}";
    var upvoteStoreUrl = "{{route('upvote.store')}}";
    var upvoteDeleteUrl = "{{route('upvote.delete', false)}}";
    
    var downvoteAll = "{{route('downvote.index', false)}}";
    var downvoteStoreUrl = "{{route('downvote.store')}}";
    var downvoteDeleteUrl = "{{route('downvote.delete', false)}}";
    
    var commentStoreUrl = null;
    var commentDeleteUrl = "{{route('comment.delete', false)}}";

    var categoryStoreUrl = "{{route('category.store')}}";

    // Post Generation Related Functionallities
    var baseURL = "{{asset("/")}}";
    var currentPage = 0;
    var lastPage = 0; 
    var currentUrl = null;
    var canRun = true;
    var stopLoading = false;
    function getPosts(url, page){
      $('#posts-loading-icon').show();
      $.ajax({
              url: url, 
              data :{
                page : page,
              },
              method : 'GET',
              dataType : 'json',
              success: function(response){
                 currentPage = response.data.meta.current_page;
                 lastPage = response.data.meta.last_page;
                 $('.posts-section').append(response.data.content);
                 $('[data-toggle="popover"]').popover({html : true});
                 $('#posts-loading-icon').hide();
                 canRun = true;
                 if(currentPage >= lastPage){
                    stopLoading = true;
                 }
                 // console.log(canRun);
                 initializeShareCount();
              },
              error: function(errorResponse){
                
              }
          });
    }
    
    // Under Construction

    function clearPostSection(){
      $('.posts-section').html('');
      stopLoading = false;
      currentPage = 0;
      lastPage = 0;
    }
    function showMorePosts(){
      // console.log(orderBy);
      // console.log(currentPage);
      // console.log(currentUrl);
      currentPage += 1;
      getPosts(currentUrl, currentPage); 
    }
    // Post Generation

    $(document).ready(function(){
        // Popover Initialize
        
        $('body').on('click', function (e) {
            if ($(e.target).data('toggle') !== 'popover'
                && $(e.target).parents('.popover.in').length === 0) { 
                $('[data-toggle="popover"]').popover('hide');
            }
        });

        $("textarea").keyup(function(e) {
            while($(this).outerHeight() < this.scrollHeight + parseFloat($(this).css("borderTopWidth")) + parseFloat($(this).css("borderBottomWidth"))) {
                $(this).height($(this).height()+1);
            };
        });

        // Added By Joy 6/7/16
        $('#example-getting-started').multiselect({
              enableFiltering: true,
              enableCaseInsensitiveFiltering: true,
              nonSelectedText: 'Select Category',
              templates : {
                filter: '<li class="multiselect-item filter"><div class="input-group"><span class="input-group-btn"><button id="category-add-btn" data-type="create" class="btn btn-default" type="button"><i class="glyphicon glyphicon-plus"></i></button></span><input class="form-control multiselect-search" type="text"></div></li>',
              }, 
               maxHeight: 150,
              filterBehavior: 'both',
              onChange: function(element, checked) {
                  var selected = $('#example-getting-started option:selected');
                  if(selected.length > 3)
                  {
                      $("#example-getting-started").multiselect('deselect', element.val());
                  }
              }
        });
        $('#save-category-selection').multiselect({
              enableFiltering: true,
              enableCaseInsensitiveFiltering: true,
              nonSelectedText: 'Select Category',
              templates : {
                filter: '<li class="multiselect-item filter"><div class="input-group"><span class="input-group-btn"><button data-type="save" class="btn btn-default" type="button"><i class="glyphicon glyphicon-search"></i></button></span><input class="form-control multiselect-search" type="text"></div></li>',
              }, 
               maxHeight: 150,
              filterBehavior: 'both',
              onChange: function(element, checked) {
                  var selected = $('#save-category-selection option:selected');
                  if(selected.length > 3)
                  {
                      $("#save-category-selection").multiselect('deselect', element.val());
                  }
              }
        });
        
        $('#select-category-profile').multiselect({
            enableFiltering: true,
            enableCaseInsensitiveFiltering: true,
            nonSelectedText: 'Select Category',
            maxHeight: 150,
            filterBehavior: 'both',
            templates : {
                filter: '<li class="multiselect-item filter"><div class="input-group"><input class="form-control multiselect-search" type="text"></div></li>',
            } 
        });

        // Preview Of Youtube
        var canScrape = true;
        $('textarea').keyup(function(e){
            e.preventDefault();
            var text = $(this).val();
            var res = text.match(/(?:http|https)?(?:\:\/\/)?(?:www.)?(([A-Za-z0-9-]+\.)*[A-Za-z0-9-]+\.[A-Za-z]+)(?:\/.*)?/g);
        
            if(res != null){
              if(canScrape){
                  if(res[0].match('youtube.com')){
                        var l = getParameterByName('v',res[0]);
                        $('#loading-icon').fadeIn(200);
                        $.ajax({
                            url : "{{route('scrape')}}",
                            data : {
                              url : res[0]
                            },
                            v : l,
                            success:function(response){

                                var title = response.title;
                                var preview = "<div class=\"link\"><a target=\"_blank\" href=\""+res[0]+"\"style=\"text-decoration:none; color:black;\"><div class=\"dtc v-mid\"><iframe width=\"300\" src=\"http://www.youtube.com/embed/"+this.v+"?autoplay=0\"></iframe></div><div class=\"dtc v-mid pl3\"><p class=\"lh-copy\"><h3>"+title+"</h3><p></p></p></div></a></div>";
                                $('#preview').html(preview);
                                $('#loading-icon').fadeOut(200);
                                canScrape = false;
                            },
                            error:function(response){

                            }
                        });
                    }
                    else if(res[0].match('ted.com')){
                        $('#loading-icon').fadeIn(200);
                        $.ajax({
                            url : "{{route('scrape')}}",
                            data : {
                              url : res[0]
                            },
                            success:function(response){

                                var title = response.title;
                                var embed_url = response.embed_url;
                                var preview = "<div class=\"link\"><a target=\"_blank\" href=\""+res[0]+"\"style=\"text-decoration:none; color:black;\"><div class=\"dtc v-mid\"><iframe width=\"300\" src=\""+embed_url+"\"></iframe></div><div class=\"dtc v-mid pl3\"><p class=\"lh-copy\"><h3>"+title+"</h3><p></p></p></div></a></div>";
                                $('#preview').html(preview);
                                $('#loading-icon').fadeOut(200);
                                canScrape = false;
                                
                            },
                            error:function(response){
                                // console.log('ted.com Scrapping Error');
                            }
                        });
                    }
                    else if(res[0].match('medium.com')){
                        $('#loading-icon').fadeIn(200);
                        $.ajax({
                            url : "{{route('scrape')}}",
                            data : {
                              url : res[0]
                            },
                            success:function(response){

                                var title = response.title;
                                var image = response.image;
                                var description = response.description;
                                var preview = "<div class=\"link\"><a target=\"_blank\" href=\""+res[0]+"\"style=\"text-decoration:none; color:black;\"><div class=\"dtc v-mid\"><img src=\""+image+"\" alt=\"\" class=\"img-responsive\" /></div><div class=\"dtc v-mid pl3\"><p class=\"lh-copy\"><h3>"+title+"</h3><p>"+description+"</p></p></div></a></div>";
                                $('#preview').html(preview);
                                $('#loading-icon').fadeOut(200);
                                canScrape = false;

                                
                            },
                            error:function(response){
                                // console.log('medium.com Scrapping Error');
                            }
                        });
                    }
              } 
            }
            else{
                $('#preview').html('');
                canScrape = true;
            }    
        });
        
        // Post Delete Releated Functiona. 
        var deletePostBaseUrl = "{{route('post.delete', false)}}";
        var deletePostRealUrl = deletePostBaseUrl+0;
        var postDivRef = null;
        $(document).on('click', '.delete-button', function(){
              deletePostRealUrl = deletePostBaseUrl+'/'+$(this).data('post-id');
              postDivRef = $('.post_'+$(this).data('post-id'));
              // console.log(postDivRef.html());
        });

        $('.delete-confirm-button').click(function(){
            // console.log(deletePostRealUrl);

             // return;
              $.ajax({
                  url : deletePostRealUrl,
                  type : 'delete',
                  success:function(response){
                      postDivRef.fadeOut(200);
                      $('#deleteModal').modal('toggle');
                  },
                  error:function(response){

                  }
              });
        });
      
        $(window).scroll(function(){
            if(canRun && !stopLoading){
                if($(window).scrollTop() + $(window).height() > $(document).height() - 100) {
                  canRun = false;
                  // console.log(currentUrl);
                  showMorePosts();
                }
            }
            
            
        });

        // Category Wise Search Initialize
        $(document).on('click','.category-search-item', function(){
            var category = $(this).data('category-name');
            currentUrl = baseURL + "api/posts/"+category;
            $('#category-search-result').parent().removeClass('open');
            // console.log(currentUrl);
            clearPostSection();
            showMorePosts();
        });
        $('.user-category-search-item').click(function(){
            var category = $(this).data('category-name');
            currentUrl = baseURL + "api/profile/posts/"+category;
            // console.log(currentUrl);
            clearPostSection();
            showMorePosts();
            return false;
        });


        $('.order-by-btn').click(function(){

            clearPostSection();
            orderBy = $(this).data('order-by');
            showMorePosts();
        });

        // Facebook Share 
        $(document).on('click','.share-btn', function(){
            link = $(this).data('href');
            // link = 'http://legaats.iitds.win';
            
            // console.log(link);
            FB.ui(
            {
              method: 'share',
              href: link
            }, function(response){
                // console.log(response);
                // console.log("After Response");

            });
            return false;
        });
        

       
        $(document).on('click','.unseen', function(){
            var notification_id = $(this).data('notification-id');
            // console.log(notification_id);
             $.ajax({
                url : "{{route('notification.seen', false)}}/"+notification_id,
                dataType: 'json',
                notification : $(this),
                success:function(response){
                    // console.log(response);
                },
                error:function(response){
                    // console.log(response);
                }
              });
        });


        // Share Via Email Form Submit
         $(document).on('click','.shareViaEmailbutton', function(){
            var post_id = $(this).data('post-id');
           
             $.ajax({
                url : "{{route('share.via.email.create', false)}}/"+post_id,
                dataType: 'html',
                success:function(response){
                    $('.share-via-email-modal-body').html(response);
                   
                },
                error:function(response){
                    // console.log(response);
                }
              });
        });

        $(document).on('submit','#shareViaEmailForm', function(){
            var formData = $(this).serialize();
            console.log(formData);
             $.ajax({
                url : $(this).attr('action'),
                data : formData,
                type : 'POST', 
                dataType: 'json',
                success:function(response){
                    $('#shareViaEmail').modal('toggle');
                      var email = response.data.email;
                      var subject = response.data.subject;
                      var emailBody = response.data.body;
                      // var attach = 'path';
                      document.location = "mailto:"+email+"?subject="+subject+"&body="+emailBody;
                },
                error:function(response){
                    // console.log(response);
                    
                }
              });
             return false;
        });



        // Catgeory Search Related Codes
        $('#search-category-name').keyup(function(){
            var categoryName = $(this).val();

            if(categoryName.length < 1){
              $('#category-search-result').parent().removeClass('open');
              return;
            }
            $.ajax({
              url : "{{route('search.category')}}",
              data : {key : categoryName},
              type : 'POST', 
              dataType: 'json',
              success:function(response){

                  var listItem = "";
                  var categories = response.data.categories;
                  var users = response.data.users;

                  $('#category-search-result').html('');
                  $('#category-search-result').append('<li><a href="#"><i class="glyphicon glyphicon-list-alt"></i> Categories </a><hr></li>');

                  if(categories.length == 0){
                      $('#category-search-result').append('<li><a href="#">No results</a></li>');
                  }
                  else{
                    for (var i = 0; i < categories.length; i++) {

                          url = baseURL+"category/"+categories[i].id+"/feed";
                         listItem += "<li><a href='"+url+"'>"+categories[i].name+"</a></li>"; 
                      
                    };
                    $('#category-search-result').append(listItem);
                  }
                  listItem = "";
                  $('#category-search-result').append('<li><a href="#"><i class="glyphicon glyphicon-user"></i> Peoples </a></li><hr>');
                  if(users.length == 0){
                    $('#category-search-result').append('<li><a href="#">No results</a></li>');
                  }
                   else{
                    for (var i = 0; i < users.length; i++) {

                        url = baseURL+"other/profile/"+users[i].id;
                         listItem += "<li><a href='"+url+"'><img src='"+users[i].avatar+"' width='20'/> "+users[i].fullname+"</a></li>"; 
                      
                    };
                    $('#category-search-result').append(listItem);
                  }

                  $('#category-search-result').parent().addClass('open');
                  
              },
              error:function(response){
                   $('#category-search-result').html('<li><a href="#">No results</a></li>');
                  
              }
            });
        });

        // glyphicon-plus
        // Category Add Option
        // $('#example-getting-started').multiselect('refresh');
        $('.filter').on('click','#category-add-btn', categoryAddFunction = function(){
            
            var formData = $(this).serialize();
            categoryName = $('.multiselect-search').val();
            formData = {
              name : categoryName
            };
            console.log(formData);
       
             $.ajax({
                url : categoryStoreUrl,
                data : formData,
                type : 'POST', 
                dataType: 'json',
                success:function(response){
                    console.log(response);
                
                    $('#example-getting-started').append($('<option>', {
                        value: response.data.id,
                        text : response.data.name
                    }));

                    $('#example-getting-started').multiselect('rebuild');
                    $('.filter').on('click','#category-add-btn', categoryAddFunction);
                   
                },
                error:function(response){
                    
                }
              });
             return false;
        });

        $('.posts-section').on('click','.show-more-text', function(){
            var postParagraph = $(this).parent();
            postParagraph.html(postParagraph.data('original-post'));
            return false;
        });

        $('.posts-section').on('click','.show-less-text', function(){
            var postParagraph = $(this).parent();
            postParagraph.html(postParagraph.data('less-post'));
            return false;
        });
    });

// Image Preview Related Functionallity
    if($('#image-preview-element').attr('src') === '')
    {
      $('#image-preview').hide();
    }
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#image-preview-element')
                    .attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
            $('#image-preview').fadeIn(200);
        }
        else
        {
           $('#image-preview').fadeOut(200);
        }
    }
    function initializeShareCount(){
      $('.fb-share-count').each(function(index){
          var link = $(this).data('href');
          // var link = 'https://www.maskcamp.com';
          $.ajax({
            url : 'https://graph.facebook.com/?id='+link,
            dataType: 'json',
            countHolder : $(this),
            success:function(response){
                if(response.shares != null){
                    this.countHolder.html(response.shares+" shares");
                }
               
            },
            error:function(response){
                // console.log(response);
            }
          });
      });
    }
  </script>
  
    

@yield('script')