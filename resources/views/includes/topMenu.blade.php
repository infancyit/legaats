<!-- start navbar -->
<nav class="navbar navbar-default" id="affix1" data-spy="affix" data-offset-top="50">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a href="{{route('wall')}}"><img src="{{asset('images/site_logo.png')}}" width="150"></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->

    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        <?php $notifications = App\Notification::getNotifications(); ?>
        <li><a href="#" class="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Notification <span class="badge">{{count($notifications)}}</span></a>
           
           <ul class="dropdown-menu" style="overflow-y:scroll;max-height:400px;">
            
            <li class="notificaion-content">
              <a align="center" href="#" class="show-more-notification">
                Show more
              </a>
            </li>
          </ul>
        </li>
        <li><a href="{{route('wall')}}">Home</a></li>
        <li><a href="{{route('queue.index')}}">For Later</a></li>
        <li><a href="{{route('profile')}}">Profile</a></li>
        <li><a href="{{route('blog.index')}}">Blog</a></li>
        <li><a href="{{route('logout')}}">Logout</a></li>
        
        
      </ul>
    </div>
    <hr><!-- /.navbar-collapse -->
     <div class="input-group col-md-4">
        <span class="input-group-addon">
          <i class="glyphicon glyphicon-search"></i>

        </span>
        <input type="text" id="search-category-name" class="form-control" placeholder="Search Profiles or Categories"/>
        
        <ul class="dropdown-menu" id="category-search-result">
          
        </ul>
      </div>
     
      <div class="input-group col-md-4">
        <ul class="nav nav-pills">
          <li role="presentation"><a href="{{route('wall')}}">What's New</a></li>
          <li role="presentation"><a href="{{route('wall.whatyoufollow')}}">What You Follow</a></li>
        </ul>
      </div>
      
   
   
  </div><!-- /.container-fluid -->  
</nav>