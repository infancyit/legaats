<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{ $title }} - {{ Config::get('customConfig.names.siteName')}}</title>
    @if(isset($post))
        <meta property="fb:app_id"          content="286508528363236" /> 
        <meta property="og:type"            content="article" /> 
        <meta property="og:url"             content="{{route('single', $post->id)}}" /> 
        <meta property="og:title"           content="Legaats" /> 
        <meta property="og:image"           content="{{asset('images/site_logo.png')}}" /> 
        <meta property="og:description"    content="{{$post->text}}" />
    @endif
    <!-- Bootstrap core CSS -->

    {!! Html::style('css/bootstrap.min.css') !!}
    {!! Html::style('css/bootstrap-multiselect.css') !!}
    {!! Html::style('css/tachyons.min.css') !!}

    {!! Html::style('css/custom.css') !!}
    @yield('style')
     <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
     
     <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
     
     <style type="text/css">
     	/*Added By Joy*/
     	#loading-icon{
     		display: none;
     	}
        iframe{
            width: 100%;
            height: 100%;
            margin-top: 0.9rem; 
        }
        .blue-text{
            color: blue;
        }
     </style>   
     <div id="fb-root"></div>
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.6&appId=286508528363236";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
    window.fbAsyncInit = function(){  // this gets triggered when FB object gets initialized
            console.log("FB Object initiated");
            FB.XFBML.parse(); // now we can safely call parse method
       };
    </script>
</head>
