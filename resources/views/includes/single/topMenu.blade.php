<!-- start navbar -->
<nav class="navbar navbar-default" id="affix1" data-spy="affix" data-offset-top="50">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a href="{{route('wall')}}"><img src="{{asset('images/site_logo.png')}}" width="150"></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
       
        
        <li><a href="{{route('wall')}}">Home</a></li>
        <li><a href="{{route('queue.index')}}">For Later</a></li>
        <li><a href="{{route('profile')}}">Profile</a></li>
        <li><a href="{{route('blog.index')}}">Blog</a></li>
        <li><a href="{{route('login')}}">Log In</a></li>
        
        
      </ul>
    </div>
    <hr><!-- /.navbar-collapse -->
   
  </div><!-- /.container-fluid -->  
</nav>