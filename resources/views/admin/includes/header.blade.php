<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{ $title }} - {{ Config::get('customConfig.names.siteName')}}</title>
    <!-- Bootstrap core CSS -->

    {!! Html::style('bootstrap_3_3_7/css/bootstrap.min.css') !!}
    {!! Html::style('css/bootstrap-multiselect.css') !!}

    {!! Html::style('bootstrap_3_3_7/css/custom.css') !!}

     <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
     
     <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
    @yield('style')
    
</head>
