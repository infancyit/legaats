<br><br><br><br>
<!-- javascript-->
  <script src="{{asset('js/jquery-2.1.1.min.js')}}"></script>
  <script src="{{asset('js/jquery-ui.min.js')}}"></script>
  
  <script src="{{asset('bootstrap_3_3_7/js/bootstrap.min.js')}}"></script>
  <script src="{{asset('js/bootstrap-multiselect.js')}}"></script>

  <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
@yield('script')