<!DOCTYPE html>
<html lang="en">
@include('admin.includes.header')

<body>
<div class="container">
  

  <div class="page-header">
    <h1 align="right">Legaats  <small>Admin Panel</small></h1>
  </div>

@include('admin.includes.topMenu')

@yield('content')
</div>

@include('admin.includes.footer')

</body>
</html>