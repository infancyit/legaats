@extends('admin.layouts.default')

@section('content')
	<h1 align="center">All Users</h1>
	<div class="container">
		@include('admin.includes.alert')
		<table id="example" class="table table-bordered" cellspacing="0" width="100%">
	        <thead>
	            <tr>
	                <th>User ID</th>

	                <th>Fullname</th>
	                <th>Email</th>
	                <th>Gender</th>
	                <th>Age</th>
	                <th>Occupation</th>
	                <th>Actions</th>
	                <!-- <th>Salary</th> -->
	            </tr>
	        </thead>
	        <tbody>
	        	@foreach($users as $user)
	            <tr>
	                <td>{{$user->id}}</td>
	                <td>{{$user->name}}</td>
	                <td>{{$user->email}}</td>
	                <td>{{$user->gender}}</td>
	                <td>{{$user->age}}</td>
	                <td>{{$user->occupation}}</td>

	                <td>
					    <a class="btn btn-danger deleteBtn" data-toggle="modal" data-target="#deleteModal" href="{{route('admin.user.delete', $user->id)}}">Delete</a>
	                </td>
	            </tr>
	    		@endforeach
	        </tbody>
	    </table>
	</div>
	 <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	    <div class="modal-dialog" role="document">
	      <div class="modal-content">
	        <div class="modal-header">
	          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	          <h4 class="modal-title" id="myModalLabel">
	              Are you sure you want to delete this post ? 
	          </h4>
	        </div>
	        <div class="modal-body">

	        </div>
	        <div class="modal-footer">
	          	{!! Form::open(array('method'=> 'delete', 'class' => 'deleteForm')) !!}
                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
                {!! Form::submit('Yes, Delete', array('class' => 'btn btn-success')) !!}
                {!! Form::close() !!}
	        </div>
	      </div>
	    </div>
  </div>
@endsection

@section('script')
	<script type="text/javascript">
        var baseUrl = '{{asset('/')}}';
        $(document).ready(function() {
 
            $('#example').dataTable();
            $(document).on("click", ".deleteBtn", function() {
                var url = $(this).attr('href');
                
                $(".deleteForm").attr("action", url);
            });
            
        });
        

    </script>
@endsection