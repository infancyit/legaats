<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from coderthemes.com/lugada_1.2/layout2/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 21 Sep 2016 13:20:59 GMT -->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Coderthemes">
    <link rel="shortcut icon" href="{{asset('aboutpage/images/favicon.ico')}}">

    <title>Legaats - Build Your Legacy</title>
    <link href="//db.onlinewebfonts.com/c/4c0277bcba75a3570694a90365e79f29?family=DroidSerifW01-Regular" rel="stylesheet" type="text/css"/>
    <!-- Bootstrap core CSS -->
    <link href="{{asset('aboutpage/css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- Animate -->
    <link href="{{asset('aboutpage/css/animate.css')}}" rel="stylesheet">

    <!-- Magnific-popup -->
    <link rel="stylesheet" href="{{asset('aboutpage/css/magnific-popup.css')}}">

    <!-- Icon-font -->
    <link rel="stylesheet" type="text/css" href="{{asset('aboutpage/css/ionicons.min.css')}}">

    <!-- Custom styles for this template -->
    <link href="{{asset('aboutpage/css/style.css')}}" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->

    
    <!-- Google Analytics -->
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-84650843-1', 'auto');
ga('send', 'pageview');
</script>
<!-- End Google Analytics -->
</head>

<body>

    <!-- Pre-loader -->
    <div class="preloader">
        <div class="status">&nbsp;</div>
    </div>



    <div class="navbar navbar-custom sticky" role="navigation">
        <div class="container">
            <!-- Navbar-header -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <i class="ion-navicon"></i>
                </button>
                <!-- LOGO -->
                <a style="padding-top:0px" class="navbar-brand logo" href="about">
                    
                    <span><img style="width:55%" src="{{asset('aboutpage/images/logo.png')}}" /> 
                    </span>
                    

                </a>
                 
            </div>
            <!-- end navbar-header -->

            <!-- menu -->
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="#home_top">Home</a>
                    </li>
                    <li>
                        <a href="#about">About</a>
                    </li>

                    <li>
                        <a href="#subscribe">Sign Up</a>
                    </li>

                </ul>
            </div>
            <!--/Menu -->
        </div>
        <!-- end container -->
    </div>
    <div class="clearfix"></div>




    <!-- HOME -->
   <!--  <section  class="" id="home_top">
        <div class="bg-overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <div class="home-wrapper">
                    	<div style="background-color:rgba(66, 72, 79, 1);display:none">
                        <h2 style="color:#38C098" class="animated fadeInDown wow" data-wow-delay=".1s">
                        	“Legaat” - A collection of photos, videos, text, and/or audio that conveys wisdom about a specific theme
                        </h2>
                        <h4 class="animated fadeInDown wow" data-wow-delay=".2s">
                        	
Legaats is a tool to collect, categorize, and share pieces of wisdom that you come across in life and online. These nuggets can be anything from a one-sentence quote to a twenty minute video. Your collection of different 'Legaats' will live on for eternity. from a one-sentence quote to a twenty minute video. Your collection of different 'Legaats' will live on for eternity. 
                        	<br/><br/>
                        	Sign up below so you can be notified when we launch!
                        </h4>
                        </div>
                        <a href="#" class="btn btn-custom btn-rnd animated fadeInDown wow" data-wow-delay=".4s">Sign Up Now!</a>
                        <div class="clearfix"></div>
                        
                    </div>
                </div>
            </div>
        </div>
    </section> -->
    <!-- END HOME -->



<section class="section" style="background-color:#42484F" id="home_top">
        <div class="container">

            <div class="row">
                <div class="col-sm-12 text-center">
                <img src="http://iit.space/legaats/images/bg.jpg" style="
    margin-top: 3%;
    margin-bottom: 6%;width:90%
">

<div class="row">
                <div class="col-sm-10 col-sm-offset-1 text-center">                   
<h2 style="color:#38C098;margin-top:-4%">
                        	
                           <span style="font-family:DroidSerifW01-Regular,droid-serif-w01-regular, droid-serif-w02-regular, droid-serif-w10-regular, serif; font-size:24px;">“Legaat”  - A collection of photos, videos, text, and/or audio that conveys wisdom about a specific theme</span>
                        </h2>
                        <br/>
                        <h4 style="color:white" >

                        	Join Legaats to collect, categorize, and share the most important pieces of wisdom you come across in life and online. Group pieces of wisdom under specific themes to create different ‘Legaats.’ Your collection of different ‘Legaats’ will live on for eternity. 
                        	<br/><br/>
                        	Sign up below so you can be notified when we launch!
                        </h4>
                        </br>
                        <a href="#subscribe" class="btn btn-custom btn-rnd " style="text-transform: capitalize;" >Sign Up Now!</a>
                        <div class="clearfix"></div>
                 </div>
     </div>       
                        
                   
                </div>
            </div>

           


        </div>
    </section>




    <!-- FEATURES-1 -->
    <section class="section" id="about">
        <div class="container">

            <div class="row">
                <div class="col-sm-12 text-center">
                
                    <h3 class=" font-bold">About Us</h3>
                    <div class="title-hr"></div>

                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-3">
                            <p class="sub-title">
With over 3 billion people online today, why are we only exposed to the wisdom, teachings and life lessons of a very select few business leaders, world leaders, celebrities and public writers?</br></br>
You, too, have unique learnings and wisdom to share, and so do others around you. So why is this wisdom not out in the open?
</br></br>
The answer: no one has real incentives to share their wisdom online. With Legaats, that’s about to change.
</br></br>
Join Legaats to collect, categorize, and share the most important pieces of wisdom you come across in life and online. Group pieces of wisdom under specific themes to create different ‘Legaats.’ Your collection of different ‘Legaats’ will live on for eternity.</br></br>
Sign up below so you can be notified when we launch!


                            </p>
                        </div>
                    </div>
                </div>
            </div>

           


        </div>
    </section>
    <!-- END FEATURES-1 -->
   
    <section class="section" id="subscribe">
        <div class="container">

            <div class="row">
                <div class="col-sm-12 text-center">
                    <h3 class="font-bold" style="font-size: 30px;">Sign Up</h3>
                    <div class="title-hr"></div>
                    <span class="error-span">
                            
                        </span>
                    <form class="sign-up-form" action="#" method="POST">
                    <div class="col-md-12">
                        
                    	<div class="form-group col-md-6">
                    		<label >Name</label>
                    		<input class="form-control" id="name" name="name" type="text" required>
                    	</div>
                    	<div class="form-group col-md-6">
                    		<label >Email</label>
                    		<input class="form-control" id="email" name="email" type="email" required>
                    	</div>

                        <?php 
                            $genders = [
                                '' => 'Select Gender',
                                'Male' => 'Male',
                                'Female' => 'Female',

                                'Transgender' => 'Transgender',
                                'I’d rather not say' => 'I’d rather not say'
                            ];

                            $ages = [
                                '' => 'Select Age',
                                'Less than 18'  => 'Less than 18',
                                '18-24' => '18-24',
                                '25-34' => '25-34',
                                '35-44' => '35-44',
                                '45-54' => '45-54',
                                '55-64' => '55-64',
                                '65+' => '65+'
                            ];
                         ?>

                            
                    	<button type="submit" class="btn btn-custom submit-btn" style="text-transform:none;">Submit</button>
                        

                    </div>
                     </form>


                </div>
            </div>

           


        </div>
    </section>

    <!-- Post Delete Modal -->
  <div class="modal fade" id="signupModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        
        </div>
        <div class="modal-body" style="height:100%;">
                
                    <span class="glyphicon glyphicon-ok" style="color:green;font-size:30px;"></span> You’re Done! <br><h1 align="center"> Thank You </h1><p align="center">You will receive an e-mail prior to our launch!</p>

                   
                    
               
        </div>
        <div class="modal-footer">
      
        </div>
      </div>
    </div>
  </div>


    




    <!-- TESTIMONIALS -->

    <section style="background:rgba(66, 72, 79, 1)" class="section bg-dark footer-cta">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 text-center">
                    <a style="color:white" href="mailto:info@legaats.com">info@legaats.com</a>
                    <p>25 Broadway New York NY United States 10005</p>
                    <a href="https://www.facebook.com/legaats/" target="_blank">
                        <li class="ion-social-facebook" data-pack="social" data-tags="like, post, share" style="display: inline-block;padding-right: 2%;color:white;font-size:18px"></li>
                    </a>
                    <a href="https://twitter.com/legaats" target="_blank">
                        <li class="ion-social-twitter" data-pack="social" data-tags="follow, post, share" style="display: inline-block;padding-right: 2%;color:white;font-size:18px"></li>    
                    </a>
                    <a href="https://www.linkedin.com/company/15074784?trk=tyah&trkInfo=clickedVertical%3Acompany%2CclickedEntityId%3A15074784%2Cidx%3A1-1-1%2CtarId%3A1473890740560%2Ctas%3Alegaats" target="_blank">
                        <li class="ion-social-linkedin" data-pack="social" data-tags="connect" style="display: inline-block;color:white;font-size:18px"></li>
                    </a>
                    
                    
                    <p>©2016 by Legaats LLC, a Delaware-based Corporation.
</p>
                </div>
            </div>
        </div>
    </section>

    <!--END TESTIMONIALS -->


    <!-- FOOTER -->
    <!-- <footer class="section bg-gray footer">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <h5>Lugada</h5>
                    <ul class="list-unstyled">
                        <li><a href="#">Home</a>
                        </li>
                        <li><a href="#">Features</a>
                        </li>
                        <li><a href="#">About Us</a>
                        </li>
                        <li><a href="#">FAQ</a>
                        </li>
                    </ul>
                </div>

                <div class="col-md-3 col-sm-6">
                    <h5>Social</h5>
                    <ul class="list-unstyled">
                        <li><a href="#">Facebook</a>
                        </li>
                        <li><a href="#">Twitter</a>
                        </li>
                        <li><a href="#">Behance</a>
                        </li>
                        <li><a href="#">Dribbble</a>
                        </li>
                    </ul>
                </div>

                <div class="col-md-3 col-sm-6">
                    <h5>Support</h5>
                    <ul class="list-unstyled">
                        <li><a href="#">Help & Support</a>
                        </li>
                        <li><a href="#">Privacy Policy</a>
                        </li>
                        <li><a href="#">Terms & Conditions</a>
                        </li>
                    </ul>
                </div>

                <div class="col-md-3 col-sm-6">
                    <h5>Contact</h5>
                    <address>
            795 Folsom Ave, Suite 600<br>
            San Francisco, CA 94107<br>
            <abbr title="Phone">P:</abbr> (123) 456-7890<br/>
            E: <a href="mailto:lugada@lugada.com">lugada@lugada.com</a>
          </address>
                </div>

            </div>

            <div class="row">
                <div class="col-sm-12">
                    <div class="footer-alt">
                        <p class="pull-right">2015 © Lugada</p>
                        <p class="logo"><i class="ion-social-buffer"></i>Lugada</p>
                    </div>
                </div>
            </div>
        </div>
    </footer> -->

    <!-- END FOOTER -->




    <!-- js placed at the end of the document so the pages load faster -->
    <script src="{{asset('aboutpage/js/jquery.js')}}"></script>
    <script src="{{asset('aboutpage/js/bootstrap.min.js')}}"></script>
    <!-- Jquery easing -->
    <script type="text/javascript" src="{{asset('aboutpage/js/jquery.easing.1.3.min.js')}}"></script>
    <script src="{{asset('aboutpage/js/wow.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('aboutpage/js/jquery.magnific-popup.min.js')}}"></script>
    <!--sticky header-->
    <script type="text/javascript" src="{{asset('aboutpage/js/jquery.sticky.js')}}"></script>
    <script src="{{asset('aboutpage/js/waypoints.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('aboutpage/js/jquery.counterup.min.js')}}" type="text/javascript"></script>


    <!--common script for all pages-->
     <script src="{{asset('aboutpage/js/jquery.app.js')}}"></script>

    <script type="text/javascript">
        jQuery(document).ready(function($) {
            $('.counter').counterUp({
                delay: 100,
                time: 1200
            });
        });
    </script>

    @yield('script')

</body>


<!-- Mirrored from coderthemes.com/lugada_1.2/layout2/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 21 Sep 2016 13:21:19 GMT -->
</html>