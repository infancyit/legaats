@extends('layouts.single')
    @section('content')
                
            <?php use Carbon\Carbon; ?>
            <div class="row">
                <div class="col-md-8 main">
                    <div class="row">
                        <div class="col-md-12">
                            <!-- Start Cart content -->
                           
                            <article class="br2 ba  b--black-10 mv8 w-100 w-50-m  mw0 center">
                                  
                                    <div style="padding:1.5rem;">
                                        <div class="dtc v-mid">
                                            <img src="{{asset($post->user->avatar)}}" alt="" class="img-circle" width="60" height="60"/>
                                        </div>
                                        <div class="dtc v-mid pl3">
                                            <p class="lh-copy">
                                              <a target="_blank" href="{{route('other.profile', $post->user_id)}}">{{$post->user->fullname}}</a> <!-- <span class="light-text">in</span> <a href="#">FuckHard18</a> -->
                                              
                                              <br><span class="light-text"><a href="{{route('single', $post->id)}}">{{Carbon::parse($post->created_at)->diffForHumans()}}</a></span>



                                              
                                              <br><span class="light-text">{{$post->allCategories}}</span>
                                            </p>
                                        </div>
                                    </div>

                                    <div style="padding-left:1.5rem;padding-right:1.5rem;">
                                        <?php 
                                            $urlPattern = "/(?:http|https)?(?:\:\/\/)?(?:www.)?(([A-Za-z0-9-]+\.)*[A-Za-z0-9-]+\.[A-Za-z]+)(?:\/.*)?/im";
                                         ?>
                                        <h4>{!!preg_replace($urlPattern, "<a target=\"_blank\" href=\"$0\">$0</a>", $post->text)!!}</h4>
                                        <!-- <h3 class="light-text bold-text"> {{$post->text}}</h3> -->
                                        <!-- <a href="#">Read More..</a> -->
                                        @if($post->image)
                                            <img src="{{asset($post->image->url)}}" style="width:100%;" class="img-responsive"><br>
                                        @endif
                                    </div>
                                    <br>
                                    @if($post->scrape)
                                        @include('sections.scrape')
                                    @endif  
                                    
                              
                            </article>
                            <br>
                        
                            <!-- End Card Content -->

                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="right">

                           
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                
          

             
             
@stop


@section('script')

    <script type="text/javascript">
        $(document).ready(function(){
            var baseURL = "{{asset("/")}}";
            var saveButtonReference = null;
            $('#photo-file-btn').click(function(){
                $('#photo').trigger('click');
                console.log('CLick');
                return false;
            });
            $('#photo').change(function(){
                var fileName = $(this).val();
                $('.image-preview').html('File Selected');
            });


            $('#post-btn').click(function(){
                $('#post-form').trigger('submit');
                return false;
            });
            $('.save-button').click(function(){
                var postId = $(this).data('post-id');
                $('.save-changes-btn').data('post-id', postId);
                saveButtonReference = $(this);
            });


            $('.save-changes-btn').click(function(){

                    var postId = $(this).data('post-id');
                    var categories = new Array();//$("input[name='category_id[]']:checked").serializeArray();
                    // console.log(postId);
                    // console.log($('.category-input').val());
                    $("input[name='category_id[]']:checked").each(function(i) {
                        categories.push($(this).val());
                    });
                    if(categories.length > 3){
                        var message = "You can select at most 3 Categories";
                        var success = '<div class="alert alert-danger alert-dismissable fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+message+'</div>'
                        $('#error').html(success);
                        return;
                    }
                    var requestUrl = baseURL+"savedpost/store";
                    // categories =  JSON.stringify(categories);
                    // console.log(categories);
                    // return;
                    // return;
                    // Ajax Call
                    $.ajax({
                        url: requestUrl, 
                        data :{
                            post_id : postId,
                            category_id : categories
                        },
                        method : 'POST',
                        dataType : 'json',
                        success: function(response){
                           
                            // var message = response.data.message;
                            // var success = '<div class="alert alert-success alert-dismissable fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+message+'</div>'
                            // this.span.html('');
                            // console.log(response);
                            // return;
                            
                            saveButtonReference.hide(500);
                            $('#myModal').modal('toggle');
                            $("input[name='category_id[]']").attr('checked', false);
                            $('#error').html('');
                        
                        },
                        error: function(errorResponse){
                           
                            // var message = "";
                            var messages = jQuery.parseJSON(errorResponse.responseText);
                            console.log(messages);
                            // if(messages.error.http_status == 400){
                            //      $.each(messages.error.message, function(i, item) {
                            //         message += item+"<br>";
                            //     })
                            // }
                            // else
                            // {
                            //     message = messages.error.message;
                            // }
                           
                            // var error = '<div class="alert alert-danger alert-dismissable fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+message+'</div>';
                            // this.span.html('');
                            // this.span.append(error);
                        }
                    });
                    // Ajax Call End 

                    return false;

            });


            $('.queue-button').click(function(){

                    var postId = $(this).data('post-id');
                    
                    var requestUrl = baseURL+"queue/store/"+postId;
         
                    // Ajax Call
                    $.ajax({
                        url: requestUrl, 
                        data :{
                        },
                        method : 'POST',
                        dataType : 'json',
                        queueButton : $(this),
                        success: function(response){
                           
                            // var message = response.data.message;
                            // var success = '<div class="alert alert-success alert-dismissable fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+message+'</div>'
                            // this.span.html('');
                            console.log('success');
                            this.queueButton.hide(500);
                        
                        },
                        error: function(errorResponse){
                           
                            // var message = "";
                            // var messages = jQuery.parseJSON(errorResponse.responseText);
                            // console.log(messages);
                            // if(messages.error.http_status == 400){
                            //      $.each(messages.error.message, function(i, item) {
                            //         message += item+"<br>";
                            //     })
                            // }
                            // else
                            // {
                            //     message = messages.error.message;
                            // }
                           
                            // var error = '<div class="alert alert-danger alert-dismissable fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+message+'</div>';
                            // this.span.html('');
                            // this.span.append(error);
                        }
                    });

                
                    // Ajax Call End 
                    return false;

            });


        });
        
    </script>
@stop