@extends('layouts.default')
    @section('content')
        @include('includes.alert')
        <div class="row">
            <div class="col-md-8 main">
                    @include('sections.createPost')
                    <br>
                
                    <div class="row">
                        <div class="col-md-12">
                            <!-- Start Cart content -->
                            <div class="posts-section">
                                
                            </div>
                            <div class="posts-footer text-center">
                                <img id="posts-loading-icon"  src="{{asset('images/loading.gif')}}" alt="" width="60" height="60" />

                                <!-- <a class="btn show-more-btn">Show More</a> -->
                            </div>
                            
                            <!-- End Card Content -->

                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="row">
                        <div class="col-md-12">
                            @include('sections.sidebarForProfile')                   
                        </div>
                    </div>
                </div>
             
            </div>
        


         <!-- page end-->
              <!-- Modal -->
                @include('sections.modals')
                
                
@stop

@section('script')
    <script type="text/javascript">
        
        $(document).ready(function(){
            currentUrl = baseURL + "/api/profile/posts";
            $('.edit-category-form').hide();
            
            var articleReference = null;
            $('#photo-file-btn').click(function(){
                $('#photo').trigger('click');
                console.log('CLick');
                return false;
            });
            $('#photo').change(function(){
                var fileName = $(this).val();
                $('.image-preview').html('File Selected');
            });
             
            showMorePosts();

            $(document).on('click', '.discard-button', function(){
                    var savedPostId = $(this).data('save-post-id');
                    articleReference = $(".post_"+$(this).data('post-id'));
                    var requestUrl = baseURL+"savedpost/delete/"+savedPostId;
                    // Ajax Call
                    $.ajax({
                        url: requestUrl, 
                        data :{
                        },
                        method : 'Delete',
                        dataType : 'json',
                        success: function(response){
                            articleReference.hide(500);                
                        },
                        error: function(errorResponse){
                           
                        }
                    });
                    // Ajax Call End 
                    return false;
            });

         


            // Edit Category
            var edit_category_id = null;
            var edit_form = $('.edit-category-form');

            $('.edit_category_btn').click(function(){
                edit_category_id = $(this).parent().data('category-id');

                var name = $(this).parent().parent().find('a').eq(0).html();
                edit_form.find('input[name="name"]').val(name);
                $('.edit-category-form').fadeIn(200);
                return false;
            });

            $('.edit-category-form').submit(function(){

                var requestUrl = $(this).attr('action')+"/"+edit_category_id;
                var formData = $(this).find("input").serializeArray();
                // console.log(formData);
                // Ajax Call
                    $.ajax({
                        url: requestUrl, 
                        data : formData,
                        method : 'PUT',
                        dataType : 'json',
                        span : $(this).parent().find('.error'),
                        success: function(response){
                           
                            // var message = response.data.message;
                            // var success = '<div class="alert alert-success alert-dismissable fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+message+'</div>'
                            // this.span.html('');
                            console.log('success');
                            location.reload();
                        
                        },
                        error: function(errorResponse){
                           
                            var message = "";
                            var messages = jQuery.parseJSON(errorResponse.responseText);
                            console.log(messages);
                            if(messages.http_status == 400){
                                 $.each(messages.error, function(i, item) {
                                    message += item+"<br>";
                                })
                            }
                            else
                            {
                                message = messages.error;
                            }
                           
                            var error = '<div class="alert alert-danger alert-dismissable fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+message+'</div>';
                            this.span.html('');
                            this.span.append(error);
                        }
                    });

                
                    // Ajax Call End

                return false;
            });
            
            // Profile Timeline Related Codes

            $('#select-category-profile').change(function(){
                currentUrl = baseURL+"api/profile/"+$(this).val()+"/posts";
                
                clearPostSection();
                showMorePosts();
            });

            $('.timeline-select').click(function(){

                timeline = $(this).data('timeline');
                currentUrl = baseURL+"api/profile/"+$('#select-category-profile').val()+"/posts/"+timeline;

                console.log(currentUrl);
                clearPostSection();
                showMorePosts();
                return false;
            });
            
        });
        
    </script>
@stop