<!DOCTYPE html>
<html lang="en">
@include('includes.header')

<body >
<section id="container" class="sidebar-closed">
    @include('includes.topMenu')
    
 	
 	<div class="container">
		@yield('content')
	</div>


    @include('includes.footer')
</section>




</body>
</html>