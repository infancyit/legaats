@foreach($posts as $post)
	<article class="br2 ba  b--black-10 mv8 w-100 w-50-m  mw0 center post_{{$post->id}}">
	  	<div style="padding:1.5rem;">
	  		<!-- Action Menu -->
	  		@if($post->isOwner)
				@include('sections.post_actions')
			@endif
			<!-- Action Menu End -->
		  	<div class="dtc v-mid">
			    <img src="{{asset($post->user->avatar)}}" alt="" class="img-circle" width="60" height="60"/>
			    <div class="text-center">
			    	@if($post->isFollowUser)
                	<a href="#" class="unfollow-user-btn text-center" data-user-id="{{$post->user_id}}" data-follow-user-id="{{$post->isFollowUser->id}}">Unfollow</a>
			    	@else
                	<a href="#" class="follow-user-btn text-center" data-user-id="{{$post->user_id}}">Follow</a>

			    	@endif
			    </div>

			</div>
			<div class="dtc v-mid pl3">
			    <p class="lh-copy">
			      <a target="_blank" href="{{route('other.profile', $post->user_id)}}">{{$post->user->fullname}}</a> <!-- <span class="light-text">in</span> <a href="#">FuckHard18</a> -->
			      
			      <?php 
			      		$date = Carbon::parse($post->created_at);
			       ?>
			      <br><span class="light-text"><a target="_blank" href="{{route('single', $post->id)}}">{{ ($date->diffInDays() < 14) ? $date->diffForHumans() : $date->toFormattedDateString()}}</a></span>
			      <br><span class="light-text">{{$post->allCategories}}</span>
			      
			    </p>
			</div>
	  	</div>

		<div style="padding-left:1.5rem;padding-right:1.5rem;">
		  	<?php 
		  		$urlPattern = "/(?:http|https)?(?:\:\/\/)?(?:www.)?(([A-Za-z0-9-]+\.)*[A-Za-z0-9-]+\.[A-Za-z]+)(?:\/.*)?/im";
		  	 	$original_post = preg_replace($urlPattern, "<a target=\"_blank\" href=\"$0\">$0</a>", $post->text);
		  	 	$original_post = nl2br($original_post);

		  	 	$limit = 500;
		  		if(strlen($post->text) > $limit){
		  			$text = str_limit($post->text, $limit);
		  			$text = preg_replace($urlPattern, "<a target=\"_blank\" href=\"$0\">$0</a>", $text);
		  			$text = nl2br($text);
		  			$text .= '<br><a href="#" class="show-more-text">...show more</a>'; 
		  			$original_post .= '<br><a href="#" class="show-less-text">show less</a>';
		  		}
		  		else{
		  			$text = $original_post;
		  		}
		  	 ?>
		  	<p data-original-post="{{$original_post}}" data-less-post="{{ $text }}">{!! $text !!}</p>
		  
		  	@if($post->image)
		  		<img src="{{asset($post->image->url)}}" style="width:100%;" class="img-responsive"><br>
		  	@endif
		</div>
		<br>
		@if($post->scrape)
				@include('sections.scrape')
		@endif
		@if(!$post->isOwner)
		<div style="padding:1.5rem;">
			
	            @if(!$post->isQueued)
	            <a class="btn btn-default my-btn queue-button" data-post-id="{{$post->id}}" href="#" style="">For Later</a>
	            @endif
	            @if(!$post->isSaved)
	            <a class="btn btn-default my-btn save-button" data-toggle="modal" data-target="#myModal" data-post-id="{{$post->id}}" href="#" style="">Save</a>
	            @endif
	      	
	  	</div>
	  	@endif
	  	
		@include('sections.like_comment_button_section')
		
		@include('sections.comment') 	  
	</article>
@endforeach