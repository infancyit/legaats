<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="keyword" content="">
    <link rel="shortcut icon" href="img/favicon.png">

    <title>{{ $title }} - {{ Config::get('customConfig.names.siteName') }}</title>

    {!! Html::style('css/bootstrap.min.css') !!}
    {!! Html::style('css/bootstrap-reset.css') !!}
    {!! Html::style('css/custom.css') !!}
    <style type="text/css">
        .facebook-login-btn{
            background: #3C5B9A;
            box-shadow: 0 0;
            
            font-weight: bold;
        }
        .facebook-signup-btn{
            background: #3C5B9A;
            box-shadow: 0 0;
            
            font-weight: bold;
        }
        .signin-form a{
           color: white;
        }
        .signin-form{
            max-width: 300px;
            margin: 15em auto 0%;
            background: #fff;
        }
        .signin-form p{
            padding-right: 1em;
            font-size: 1.5em;
        }

    </style>
</head>

<body class="login-body">
<div class="container">
    <div class="signin-form">
        
    
    <img style="width:100%;" src="{{asset('images/site_logo.png')}}">
    <!-- <p class="text-right">Build Your Legacy</p> -->
    <div class="login-wrap">
        @include('includes.alert')
        <!--  
        {!! Form::text('email', '', array('class' => 'form-control', 'placeholder' => 'Email Address', 'autofocus')) !!}
        {!! Form::password('password', array('class' => 'form-control', 'placeholder' => 'Password')) !!}
        <label class="checkbox">
                        <span class="pull-left">

                            <a data-toggle="modal" href="{{route('user.create')}}"> Want to be a member ?</a>
                            
                        </span>
                        <span class="pull-right">


                            <a data-toggle="modal" href="#myModal"> Forgot Password ?</a>
                            
                        </span>
        </label> 
         {!! Form::submit('Log in', array('class' => 'btn btn-lg btn-login btn-block')) !!} -->
        
        <a class="btn facebook-login-btn pull-right" href="{!! route('dologin') !!}">Sign In With Facebook<!-- <img height="50" src="{{asset('img/facebook.png')}}"> --></a>
    </div>
    </div>

    <!-- Modal -->
    <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Forgot Password ?</h4>
                </div>
                <div class="modal-body">
                    <p>Enter your e-mail address below to reset your password.</p>


                    {!! Form::open(array('action' => 'RemindersController@postRemind', 'method' => 'post')) !!}

                    {!! Form::email('email', '', array('class' => 'form-control placeholder-no-fix', 'placeholder' => 'Email Address', 'autocomplete'=>'off')) !!}




                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>

                    {!! Form::submit('Submit', array('class' => 'btn btn-success')) !!}

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <!-- modal -->

</div>

<!-- js placed at the end of the document so the pages load faster -->
{!! Html::script('js/jquery-2.1.1.min.js') !!}
{!! Html::script('js/bootstrap.min.js') !!}

</body>
</html>
