@extends('layouts.default')
    @section('content')
        	@include('includes.alert')
	     	<div class="row">
				<div class="col-md-8 main">
					<div class="row">
						<div class="col-md-12">
							<!-- Start Card content -->
							<article class="br2 ba  b--black-10 mv8 w-100 w-50-m  mw0 center">
								<div class="form">

									{!! Form::open(array('route' =>  'blog.store' ,'class' => 'form-horizontal', 'files' => true)) !!}
									  <div class="form-group">
									    <label for="inputEmail3" class="col-sm-2 control-label">Post Title</label>
									    <div class="col-sm-10">

									    	{!! Form::text('title', null, array('class'=> 'form-control my-form-control', 'placeholder' => 'Post Title', 'required')) !!}
									     
									     </div>
									  </div>
									  
									  <div class="form-group">
									    <label for="textarea" class="col-sm-2 control-label">Description</label>
									    <div class="col-sm-10">
									    	{!! Form::textarea('description', null, array('class'=> 'form-control my-form-control', 'placeholder' => 'Description', 'required')) !!}
									    	
									    </div>
									  </div>
									  

									  <div class="form-group">
									    <div class="col-sm-offset-2 col-sm-10">
									      	<!-- <div class="upload-button btn btn-default">Upload Image</div> -->
											<!-- <input class="btn btn-default" type="file"/> -->
									    </div>
									  </div>

									  <div class="form-group">
									    <div class="col-sm-offset-2 col-sm-10">
									      <button type="submit" class="btn btn-default my-btn">Create</button>
									    </div>
									  </div>
									{!! Form::close() !!}
								</div>
							</article>
							<!-- End Card Content -->

						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="row">
						<div class="col-md-12">
							<div class="right">

								@include('blog.rightSideBar')
							</div>
						</div>
					</div>
				</div>
			</div>
			 @include('blog.modals')
@stop


@section('script')

	<script type="text/javascript">
	    $(document).ready(function(){
	    	var baseURL = "{{asset("/")}}";
	    	var saveButtonReference = null;
	        $('#photo-file-btn').click(function(){
	            $('#photo').trigger('click');
	            console.log('CLick');
	            return false;
	        });
	        $('#photo').change(function(){
	            var fileName = $(this).val();
	            $('.image-preview').html('File Selected');
	        });


	        $('#post-btn').click(function(){
	            $('#post-form').trigger('submit');
	            return false;
	        });
	        $('.save-button').click(function(){
	        	var postId = $(this).data('post-id');
	        	$('.save-changes-btn').data('post-id', postId);
	        	saveButtonReference = $(this);
	        });


	        $('.save-changes-btn').click(function(){

	        		var postId = $(this).data('post-id');
	        		var categories = new Array();//$("input[name='category_id[]']:checked").serializeArray();
	        		// console.log(postId);
	        		// console.log($('.category-input').val());
	        		$("input[name='category_id[]']:checked").each(function(i) {
						categories.push($(this).val());
					});
					if(categories.length > 3){
						var message = "You can select at most 3 Categories";
			            var success = '<div class="alert alert-danger alert-dismissable fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+message+'</div>'
			            $('#error').html(success);
						return;
					}
	        		var requestUrl = baseURL+"savedpost/store";
	        		// categories =  JSON.stringify(categories);
	        		// console.log(categories);
	        		// return;
	        		// return;
	        		// Ajax Call
	        		$.ajax({
			            url: requestUrl, 
			            data :{
			            	post_id : postId,
			            	category_id : categories
			            },
			            method : 'POST',
			            dataType : 'json',
			            success: function(response){
			               
			                // var message = response.data.message;
			                // var success = '<div class="alert alert-success alert-dismissable fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+message+'</div>'
			                // this.span.html('');
			                // console.log(response);
			                // return;
			                
			                saveButtonReference.hide(500);
			                $('#myModal').modal('toggle');
			                $("input[name='category_id[]']").attr('checked', false);
			                $('#error').html('');
			            
			            },
			            error: function(errorResponse){
			               
			                // var message = "";
			                var messages = jQuery.parseJSON(errorResponse.responseText);
			                console.log(messages);
			                // if(messages.error.http_status == 400){
			                //      $.each(messages.error.message, function(i, item) {
			                //         message += item+"<br>";
			                //     })
			                // }
			                // else
			                // {
			                //     message = messages.error.message;
			                // }
			               
			                // var error = '<div class="alert alert-danger alert-dismissable fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+message+'</div>';
			                // this.span.html('');
			                // this.span.append(error);
			            }
			        });
					// Ajax Call End 

					return false;

	        });


			$('.queue-button').click(function(){

	        		var postId = $(this).data('post-id');
	        		
	        		var requestUrl = baseURL+"queue/store/"+postId;
	     
	        		// Ajax Call
	        		$.ajax({
			            url: requestUrl, 
			            data :{
			            },
			            method : 'POST',
			            dataType : 'json',
			            queueButton : $(this),
			            success: function(response){
			               
			                // var message = response.data.message;
			                // var success = '<div class="alert alert-success alert-dismissable fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+message+'</div>'
			                // this.span.html('');
			                console.log('success');
			                this.queueButton.hide(500);
			            
			            },
			            error: function(errorResponse){
			               
			                // var message = "";
			                // var messages = jQuery.parseJSON(errorResponse.responseText);
			                // console.log(messages);
			                // if(messages.error.http_status == 400){
			                //      $.each(messages.error.message, function(i, item) {
			                //         message += item+"<br>";
			                //     })
			                // }
			                // else
			                // {
			                //     message = messages.error.message;
			                // }
			               
			                // var error = '<div class="alert alert-danger alert-dismissable fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+message+'</div>';
			                // this.span.html('');
			                // this.span.append(error);
			            }
			        });

				
					// Ajax Call End 
					return false;

	        });


	    });
	    
	</script>
@stop