   <div class="modal fade" id="deleteConfirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	        <div class="modal-dialog">
	            <div class="modal-content">
	                <div class="modal-header">
	                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	                    <h4 class="modal-title" id="myModalLabel">Confirmation</h4>
	                </div>
	                <div class="modal-body">
	                    Are you sure to delete?
	                </div>
	                <div class="modal-footer">
	                    {!! Form::open(array('route' => array('blog.delete', 0), 'method'=> 'delete', 'class' => 'deleteForm')) !!}
	                    <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
	                    {!! Form::submit('Yes, Delete', array('class' => 'btn btn-success')) !!}
	                    {!! Form::close() !!}
	                </div>
	            </div>
	        </div>
	    </div>


	      <!-- Comment Edit Modal -->
  <div class="modal fade" id="signUpForBlogPostModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">
              Are you sure ? 
          </h4>
        </div>
        <div class="modal-body">
          {!! Form::open(['url' => route('signup.blog.mail.store'), 'files' => true]) !!}
           

            <div class="form-group">
              <label for="email">We are using this email to notify you can change it.</label>
             {!! Form::email('email', Auth::user()->email, ['class' => 'form-control', 'placeholder' => 'Email Address', 'required']) !!}
             
            </div>

            

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger my-btn close-btn" data-dismiss="modal">Discard</button>
          <button type="submit" class="btn btn-default my-btn delete-comment-confirm-button">Confirm</button>
          <!-- <button type="submit" class="btn btn-default">Send</button> -->
          {!! Form::close() !!}
        </div>
      </div>
    </div>
  </div>