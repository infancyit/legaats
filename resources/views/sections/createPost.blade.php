<article class="br2 ba  b--black-10 mv8 w-100 w-50-m  mw0 center">
    <div style="padding:1.5rem;">
        <div class="dtc v-mid">
            <img src="{{$user->avatar}}" alt="" class="img-circle" width="60" height="60" />
        </div>
        <div class="dtc v-mid pl3">
            <p class="lh-copy">
            <a href="{{route('profile')}}">{{$user->fullname}}</a> <!-- <span class="light-text">in</span> <a href="#">FuckHard18</a> -->
            
            <!-- <span class="light-text">VP/General Manager at WAAY-TV & AudiencePop, named one of Broadcasting and Cable’s Digital All-Stars</span>
            <span class="light-text">1 hour | 11 read</span> -->

            </p>

        </div>
        <img class="pull-right" id="loading-icon"  src="{{asset('images/loading.gif')}}" alt="" width="60" height="60" />
        <br>
        {!! Form::open(array('route' =>  'post.store' ,'id' => 'post-form', 'files' => true)) !!}
        <textarea name="text" class="form-control post" placeholder="Whats on your mind today ?" required></textarea>
           
        <input type="file" name="photo" id="photo"  style="display:none;" onchange="readURL(this);" />
        <div id="image-preview">
            <img src="" id="image-preview-element" style="width:100%;" class="img-responsive"><br>
        </div>
        <div id="preview">

        </div>
        </br>
        
        <a href="#" id="photo-file-btn"><span class="glyphicon glyphicon-camera glyphicon-camera-selected"></span></a>
        <select name="category_id[]" id="example-getting-started" multiple="multiple">
            <?php 
                $categories = App\Category::all();
             ?>
            @foreach($categories as $category)
                <option value="{{$category->id}}">{{$category->name}}</option>
            @endforeach
        </select>


        <input type="submit" class="btn my-btn btn-default" style="float:right;" value="Post"/>
        {!! Form::close() !!}
    </div>
</article>