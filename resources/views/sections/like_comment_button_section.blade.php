<div class="row">
	<div class="col-md-12 like-comment-btn-section">
		
		<hr>
	 	<div class="all-interactions">
	  			@if($post->isLiked)
		  			<a href="#" class="liked-btn" data-post-id="{{$post->id}}" data-like-id="{{$post->isLiked}}">Unlike</a>
		  		@else
		  			<a href="#" class="like-btn" data-post-id="{{$post->id}}" data-like-id="">Like</a>
		  		@endif
		  										  		
		  		
		  		
		  		<!-- Upvote -->
		  		<!-- @if($post->isUpvoted)
		  			<a href="#" class="upvoted-btn" data-post-id="{{$post->id}}" data-upvote-id="{{$post->isUpvoted}}">Unuppvote</a>
		  		@else
		  			<a href="#" class="upvote-btn" style="visibility:{{($post->isDownvoted) ? 'hidden': 'visible'}};" data-post-id="{{$post->id}}" data-upvote-id="">Upvote</a>
		  		@endif -->
				
		  		
		  		<!-- Down Vote -->
		  			
		  	<!-- 	@if($post->isDownvoted)
		  			<a href="#" class="downvoted-btn" data-post-id="{{$post->id}}" data-downvote-id="{{$post->isDownvoted}}">Undownvote</a>
		  		@else
		  			<a href="#" class="downvote-btn" style="visibility:{{($post->isUpvoted) ? 'hidden': 'visible'}};" data-post-id="{{$post->id}}" data-downvote-id="">Downvote</a>
		  		@endif -->
		  		
		  		<!-- Comment -->

		  			<a class="comment-btn" role="button" data-toggle="collapse" href="#comment_{{$post->id}}" aria-expanded="false" aria-controls="comment_{{$post->id}}">Comment</a>
	 				<div class="btn-group">
	 					<a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Share</a>
		 				<ul class="dropdown-menu">
					    	<li><a href="#" class="share-btn" data-href="{{route('single', $post->id)}}">Share with Facebook</a></li>
					    	<!-- <li><a class="shareViaEmailbutton" data-toggle="modal" data-target="#shareViaEmail" data-post-id="{{$post->id}}" href="mailto:{{Auth::user()->email}}" style="">Share By Email</a></li> -->
					    	
					    	<?php 
					    		$body = route('single', $post->id);
					    	 ?>
					    	<li><a href="mailto:?subject={{str_limit($post->text, 70).' - Legaats'}}&body={{$body}}">Share By Email</a></li>
							
						</ul>
	 				</div>
	 				
 					
	 				

	 	</div>	
	 	<hr>
		<div class="all-counts">
			<a tabindex="0" role="button" data-post-id="{{$post->id}}" data-count="{{$post->likeCount}}" data-toggle="popover" data-placement="bottom" title="Liked By" data-content="">{{$post->likeCount}} Likes</a>
			<!-- <a tabindex="0" role="button" data-post-id="{{$post->id}}" data-count="{{$post->upvoteCount}}" data-toggle="popover" data-placement="bottom" title="Upvoted By" data-content="">{{$post->upvoteCount}} Upvotes</a>
			<a tabindex="0" role="button" data-post-id="{{$post->id}}" data-count="{{$post->downvoteCount}}" data-toggle="popover" data-placement="bottom" title="Downvoted By" data-content="">{{$post->downvoteCount}} Downvote</a>
			 -->
			 <a tabindex="0" role="button" data-post-id="{{$post->id}}" class="" data-count="{{$post->commentCount}}">{{$post->commentCount}} Comments</a>
			<a data-href="{{route('single', $post->id)}}" data-post-id="{{$post->id}}" class="fb-share-count" data-count="0">0 shares</a>
		</div>
	</div>
</div>

