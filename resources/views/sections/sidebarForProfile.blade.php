<article class="br2 ba  b--black-10 mv8 w-100 w-50-m  mw0 center pa3 pa4-ns">
                              <div class="tc">
                                <img src="{{$user->avatar}}" alt="" class="img-circle" width="60" class="br-100 h4 w4 dib ba b--black-05 pa2" title="Kitty staring at you">
                                <h1 class="f3 mb2">Full Name : {{$user->fullname}}</h1>
                                <h2 class="f4 fw4 gray mt0">Email : {{$user->email}}</h2>
                              </div>
                            </article>
                            <br>

                           
                            <!-- <h3>Wee</h3> -->
                            @if(!App\BlogMailingList::isAlreadySignedUp())
                            <a class="btn btn-default signUpForBlogPostButton" data-toggle="modal" data-target="#signUpForBlogPostModal">Sign Up For Weekly Blog Post</a>
                            @else
                            <p>You Have Signed Up For Weekly Blog Post Mail</p>
                            @endif

                            @if(App\Blog::isBlogOwner())
                            <a class="btn btn-default" href="{{route('signup.blog.mail.index')}}">Mailing List</a>
                            @endif
                            <br> <br>
                            <a class="btn btn-primary" role="button" data-toggle="collapse" href="#followingCategoriesContainer" aria-expanded="false" aria-controls="followingCategoriesContainer">
                              Following Categories
                            </a>
                            <br> <br>
                            <div class="collapse" id="followingCategoriesContainer">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <?php 
                                            $following_categories = App\FollowCategory::whereUserId(Auth::user()->id)->get();

                                         ?>
                                        <tbody>
                                            @foreach($following_categories as $category)
                                            <tr>
                                                <td><a href="#" class="user-category-search-item" data-category-name="{{$category->category->url}}">{{$category->category->name}}</a></td>
                                                <td data-category-id="{{$category->category->id}}">
                                                    <button type="button" class="btn btn-danger unfollow-category-btn" data-category-id="{{$category->category->id}}" data-follow-category-id="{{$category->id}}">Unfollow</button>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <br> 
                            <?php 
                                $categories = App\Category::all();
                            ?>
                            Category : <select id="select-category-profile">
                                    <option value="0">All</option>
                               
                              @foreach($categories as $category)
                                  <option value="{{$category->id}}">{{$category->name}}</option>
                              @endforeach
                            </select>
                            <br>
                            <br>
                            <!-- Timeline -->
                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                              <?php 
                                    $year = (int) date('Y');
                                    $months = [
                                        'January',
                                        'February',
                                        'March',
                                        'April',
                                        'May',
                                        'June',
                                        'July',
                                        'August',
                                        'September',
                                        'October',
                                        'November',
                                        'December'
                                    ];
                               ?>

                               @foreach($timelines as $timeline)
                              <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingOne">
                                  <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#year_{{$timeline->year}}" aria-expanded="false" aria-controls="year_{{$timeline->year}}">
                                      {{$timeline->year}}
                                    </a>
                                  </h4>
                                </div>
                                <div id="year_{{$timeline->year}}" class="panel-collapse collapse" role="tabpanel">
                                    <div class="panel-body">
                                        <ul class="nav nav-pills nav-stacked">
                                            @foreach($timeline->months as $month)
                                                <li role="presentation"><a href="#" class="timeline-select" data-timeline="{{sprintf("%d-%02d",$timeline->year,$month)}}">{{$months[$month-1]}}</a></li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                              </div>
                              @endforeach
                            </div>
                                                       