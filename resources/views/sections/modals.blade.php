
<!-- Save Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">
           
                  <div class="dtc v-mid">
                      <img src="{{asset('images/po.jpg')}}" alt="" class="img-circle" width="60" height="60"/>
                  </div>
                  <div class="dtc v-mid pl3">
                      <p class="lh-copy">
                        <a href="#">{{$user->fullname}}</a> <!-- <span class="light-text">in</span> <a href="#">FuckHard18</a> -->
                        <br><span class="light-text">{{Carbon::parse($user->created_at)->diffForHumans()}}</span>
                      </p>
                  </div>
  
              
          </h4>
        </div>
        <div class="modal-body">
              
              <select name="save_category_id[]" id="save-category-selection" multiple="multiple">
                  <?php 
                      $categories = App\Category::all();
                   ?>
                  @foreach($categories as $category)
                      <option value="{{$category->id}}">{{$category->name}}</option>
                  @endforeach
              </select>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default my-btn close-btn" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-default my-btn save-changes-btn" data-post-id="">Save changes</button>
        </div>
      </div>
    </div>
  </div>


<!-- Post Delete Modal -->
  <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">
              Are you sure you want to delete this post ? 
          </h4>
        </div>
        <div class="modal-body">

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default my-btn close-btn" data-dismiss="modal">Discard</button>
          <button type="button" class="btn btn-default my-btn delete-confirm-button">Confirm</button>
        </div>
      </div>
    </div>
  </div>


  <!-- Comment Delete Modal -->
  <div class="modal fade" id="deleteCommentModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">
              Are you sure you want to delete this Comment ? 
          </h4>
        </div>
        <div class="modal-body">

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default my-btn close-btn" data-dismiss="modal">Discard</button>
          <button type="button" class="btn btn-default my-btn delete-comment-confirm-button">Confirm</button>
        </div>
      </div>
    </div>
  </div>


  <!-- Comment Edit Modal -->
  <div class="modal fade" id="shareViaEmail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">
              Share Via Email
          </h4>
        </div>
        <div class="modal-body share-via-email-modal-body">

        </div>
        <div class="modal-footer">
         
        </div>
      </div>
    </div>
  </div>


  <!-- Comment Delete Modal -->
  <div class="modal fade" id="signUpForBlogPostModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">
              Are you sure ? 
          </h4>
        </div>
        <div class="modal-body">
          {!! Form::open(['url' => route('signup.blog.mail.store'), 'files' => true]) !!}
           

            <div class="form-group">
              <label for="email">We are using this email to notify you can change it.</label>
             {!! Form::email('email', Auth::user()->email, ['class' => 'form-control', 'placeholder' => 'Email Address', 'required']) !!}
             
            </div>

            

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger my-btn close-btn" data-dismiss="modal">Discard</button>
          <button type="submit" class="btn btn-default my-btn delete-comment-confirm-button">Confirm</button>
          <!-- <button type="submit" class="btn btn-default">Send</button> -->
          {!! Form::close() !!}
        </div>
      </div>
    </div>
  </div>