<hr>
<div class="collapse comments" id="comment_{{$post->id}}">
  @foreach($post->comments as $comment)
  <div class="comment single_comment_{{$comment->id}}">
  	<div class="row">
  		<div class="col-md-2">
  			<img src="{{asset($comment->user->avatar)}}" alt="" class="img-circle" align="center" width="60" height="60"/>
        
  		</div>
  		<div class="col-md-10">
        @if($comment->user_id == Auth::user()->id)
        <div class="btn-group pull-right">
          <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <span class="caret"></span>
          </button>
            <ul class="dropdown-menu">
            <li><a class="comment-delete-button" data-toggle="modal" data-target="#deleteCommentModal" data-comment-id="{{$comment->id}}" href="#">Delete</a></li>
          </ul>
        </div>
        @endif
        <a href="#">{{$comment->user->fullname}}</a><br><br>
  			<p>{{$comment->text}}</p>
        <span class="light-text"><a href="#">{{Carbon\Carbon::parse($comment->created_at)->diffForHumans()}}</a></span>
  		</div>
  	</div>
  </div>
  @endforeach
  <div class="comment">
    <div class="row">
      <div class="col-md-2">
        <img src="{{asset($user->avatar)}}" alt="" class="img-circle" align="center" width="60" height="60"/>

      </div>
      <div class="col-md-10">
        
        <a href="#">{{$user->fullname}}</a><br><br>
       
          {!! Form::open(array('route' =>  ['comment.store', $post->id], 'class' => 'comment-form', 'files' => true)) !!}      
            <div class="form-group">
              
              {!! Form::textarea('text', null, array('class'=> 'form-control','rows' => '2', 'placeholder' => 'Write Something......', 'required')) !!}
             
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-default">Comment</button>
            </div>
          {!! Form::close() !!}
   
    
      </div>
    </div>
  </div>
</div>