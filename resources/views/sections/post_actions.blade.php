<div class="btn-group pull-right">
  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <span class="caret"></span>
  </button>
  	<ul class="dropdown-menu">
	 	<li><a class=""  href="{{route('post.edit',$post->id)}}">Edit</a></li>
	 	<li><a class="delete-button" data-toggle="modal" data-target="#deleteModal" data-post-id="{{$post->id}}" href="#" style="">Delete</a></li>
	</ul>
</div>