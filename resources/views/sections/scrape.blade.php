<div class="link">
		<a target="_blank" href="{{$post->scrape->link}}" style="text-decoration:none; color:black;">
  		@if($post->scrape->image)
        <div class="dtc v-mid">
            @if($post->scrape->type === 'normal')
            <img src="{{$post->scrape->image}}" alt="" class="img-responsive" />
            @elseif($post->scrape->type === 'youtube')
            <iframe class="img-responsive"
                src="http://www.youtube.com/embed/{{$post->scrape->youtube}}?autoplay=0" allowfullscreen>
            </iframe>
            @elseif($post->scrape->type === 'ted')
            <iframe class="img-responsive"
                src="{{$post->scrape->ted}}" allowfullscreen>
            </iframe>
            @endif
        </div>
        @endif
        <div class="dtc v-mid pl3">
            <p class="lh-copy">
                 <h3>{{$post->scrape->title}}</h3>
                @if($post->scrape->type === 'normal')
                    <p>{{$post->scrape->description}}</p>
                @endif
               

            </p>
        </div>
    </a>
</div>