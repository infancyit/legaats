@extends('about.layouts.default')

@section('content')
	

@stop


@section('script')

	<script type="text/javascript">
		

	    $(document).ready(function(){
	    	
	    	// $('.sign-up-form').submit(function(){
	    	// 	var btnStatus = $('.submit-btn');
	    	// 	console.log(btnStatus.attr('data-status'));
	    	// 	$('.error-span').html('');
	    	// 	$('.extra-section').show();
	    	// 	$('#signupModal').modal('toggle');

	    	// 	return false;
	    	// });

	    	$('.sign-up-form').submit(function(){
	    		var formData = 	$('.sign-up-form').serialize();
	    			console.log(formData);
	    			$.ajax({
	    				url : '{{route('signup.store')}}',
	    				data : formData,
	    				type : 'POST',
	    				dataType : 'json',
	    				success : function(response){
	    					console.log(response);
	    					$('.error-span').html('');
			                var success = '<span class="glyphicon glyphicon-ok" style="color:green;font-size:30px;"></span> You’re Done! <br><h1 align="center"> Thank You </h1><p align="center">You will receive an e-mail prior to our launch!</p>';

			               
			               
			              	
							// $('#signupModal').modal('toggle');
							$('.sign-up-form').trigger("reset");
							$('#signupModal').modal('toggle');
							 $('.error-span').append('');
	    				},
	    				error : function(response){
							response = jQuery.parseJSON(response.responseText);
							
	    					message = "";
	    					messages = response.error;
	    					console.log(messages);
	    					for (var i = 0; i < messages.length; i++) {
	    						message += messages[i] + '<br>';
	    					}
	    					console.log(message);
			                var error = '<div class="alert alert-danger alert-dismissable fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+message+'</div>';
			                console.log(error);
			                $('.error-span').html('');
							$('.error-span').append(error);
	    				}
	    			});
	    		return false;
	    	});
			
	    });
	    
	</script>
@stop

